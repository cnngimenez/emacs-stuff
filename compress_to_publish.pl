#!/usr/bin/env swipl

/*   compress_to_publish.pl
     Author: Gimenez, Christian.

     Copyright (C) 2024 Gimenez, Christian

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.

     3 nov 2024
*/


:- module(compress_to_publish, [
          ]).
/** <module> compress_to_publish: 

Select files and compress everything into a tar.bz2 file to upload into a public Web server.
*/

:- initialization(start, main).

accept_ext(html).
accept_ext(org).

reject_name('.').
reject_name('..').
reject_name('.git').
reject_name(secrets).

accepted_file(Filename) :-
    %% case: is a regular file and has the accepted extension
    exists_file(Filename), 
    file_name_extension(_, Ext, Filename),
    accept_ext(Ext).

accepted_dir(Name) :-
    exists_directory(Name).

select_files(LstFiles, LstAccepted) :-
    include(accepted_file, LstFiles, LstAccepted).

select_dirs(LstDirs, LstAccepted) :-
    include(accepted_dir, LstDirs, LstAccepted).

add_basedir(_, '.', '.').
add_basedir(_, '..', '..').
add_basedir(Basedir, Name, NewName) :-
    format(atom(NewName), "~s/~s", [Basedir, Name]), !.

add_basedir_all(BaseDir, LstFiles, LstOut) :-
    maplist(add_basedir(BaseDir), LstFiles, LstOut).

collect_files(Dir, Files) :-
    %% write(Dir), nl,
    directory_files(Dir, Files1),
    exclude(reject_name, Files1, Files2),
    add_basedir_all(Dir, Files2, Files3),
    
    select_files(Files3, AcceptedFiles1),
    select_dirs(Files3, Dirs),
    collect_files_in_dir(Dirs, AcceptedFiles2),
    append(AcceptedFiles1, AcceptedFiles2, Files).

collect_files_in_dir([], []) :- !.
collect_files_in_dir([Dir|DRest], Files) :-
    collect_files(Dir, Files1), 
    collect_files_in_dir(DRest, Files2),
    append(Files1, Files2, Files).

create_args([], '') :- !.
create_args([Arg|Rest], Atom) :-
    format(atom(ArgA), '"~s" ', [Arg]),
    create_args(Rest, RestA),
    atom_concat(ArgA, RestA, Atom).

create_tar(TarFile, LstFiles) :-
    create_args(LstFiles, Args),
    format(atom(Command), 'tar -cf "~s" ~s', [TarFile, Args]),
    %% write(Command).
    shell(Command).

bzip(Filename) :-
    format(atom(Command), 'bzip2 "~s"', [Filename]),
    %% write(Command).
    shell(Command).

start :-
    collect_files('.', Files),
    write('Creating salida.tar'), nl,
    create_tar('salida.tar', Files),
    write('Compressing to salida.tar.bz2'), nl,
    bzip('salida.tar').
    
