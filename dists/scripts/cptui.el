;;; cptui.el --- TUI for Cellphones  -*- lexical-binding: t; -*-

;; Copyright 2024 Christian Gimenez
;;
;; Author: Christian Gimenez
;; Maintainer: Christian Gimenez
;; Version: 0.1.0
;; Keywords: convenience
;; URL: https://gitlab.com/cnngimenez/emacs-stuff
;; Package-Requires: ((emacs "27.1"))

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.


;;; Commentary:
;; 
;; This library creates buttons and a menu buffer for touchable devices.
;; Buttons are TUI-based, as it is expected to work under termux or another
;; terminal emulator.
;;
;;; Code:

(defun cptui-insert-button (label callback)
  "Insert a button in the current buffer.
LABEL is the button string.
CALLBACK is the function to call when the buttos is tapped."
    (let ((start-column (current-column)))
      (insert-button (format "┌─%s─┐" (make-string (length label) ?─))
                     'face 'default 'action callback)
      (forward-line) (move-to-column start-column t)
      (insert-button (format "│ %s │" label)
                     'face 'default 'action callback)
      (forward-line) (move-to-column start-column t)
      (insert-button (format "└─%s─┘" (make-string (length label) ?─))
                     'face 'default 'action callback)
      (forward-line)))

(defun cptui-insert-grid-buttons (btn-list)
  "Insert buttons in grid.
BTN-LIST is a list of button data.  Each item should be a tuple of
\\=(LABEL CALLBACK).  If an element of BTN-LIST is a string, it is inserted in
the buffer in a new row.

For example:
\\='((\"label1\" #\\='my-function) (\"label2\" #\\='my-function2))"
  (when (eobp)
    ;; It is the end of buffer. Create room for the new row.
    (newline 3)
    (forward-line -3) (move-to-column 1 t))
  (let ((max-column (- (window-total-width nil t) 5)))
    (dolist (btn btn-list)
      (when (or (not btn)
                (>= (current-column) max-column)
                (> (+ (current-column) (if (stringp btn)
                                           (length btn)
                                         (length (car btn))))
                   max-column))
        ;; No more room in this column. Create room for the new row.
        (forward-line 3)
        (newline 4)
        (forward-line -3) (move-to-column 1 t))
      (if (stringp btn)
          (progn
            (forward-line 3)
            (newline 4)
            (forward-line -3)
            (insert btn)
            (newline 1)
            (move-to-column 1 t))
        (progn
          ;; It is a button information. Creates a button.
          (cptui-insert-button (car btn) (cadr btn))
          (forward-line -3) (move-end-of-line 1)
          (insert " "))))))

(defun cptui-create-menu-buffer (title btn-list)
  "Create a menu buffer with title and buttons.
Create a read-only buffer.  Show the TITLE string as its title.
Create BTN-LIST buttons (see `cptui-insert-grid-buttons')."
  (with-current-buffer (get-buffer-create "Menu")
    (setq-local buffer-read-only t)
    (let ((inhibit-read-only t))
      (delete-region (point-min) (point-max))
      (insert (propertize title 'face 'bold-italic))
      (center-line)
      (newline 1)
      (cptui-insert-grid-buttons btn-list))
    (switch-to-buffer (current-buffer))))

(provide 'cptui)
;;; cptui.el ends here
