;; [[file:firefox-update.org::*Header and license][Header and license:1]]
;;; firefox-update.el --- Update Firefox from Emacs  -*- lexical-binding: t; -*-

;; Copyright 2023 Christian Gimenez
;;
;; Author: Christian Gimenez
;; Maintainer: Christian Gimenez
;; Version: 0.1.0
;; Keywords: convenience
;; URL:
;; Package-Requires: ((emacs "27.1"))

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.
;; Header and license:1 ends here

;; [[file:firefox-update.org::*firefox-update-selected-window][firefox-update-selected-window:1]]
(defvar firefox-update-selected-window nil
  "The selected window ID.
Use `firefox-update-select-firefox' function to select a window.")
;; firefox-update-selected-window:1 ends here

;; [[file:firefox-update.org::*firefox-update-select-firefox][firefox-update-select-firefox:1]]
(defun firefox-update-select-firefox ()
  "Select a window to send the refresh key.
Basically, call \"xdotool selectwindow\" and get the results."
  (interactive)
  (with-current-buffer (get-buffer-create "*select-window*")
    (delete-region (point-min) (point-max))
    (call-process "xdotool" nil "*select-window*" nil "selectwindow")
    (goto-char (point-min))
    (setq firefox-update-selected-window (buffer-substring-no-properties (point-at-bol) (point-at-eol)))))
;; firefox-update-select-firefox:1 ends here

;; [[file:firefox-update.org::*firefox-update-refresh-firefox][firefox-update-refresh-firefox:1]]
(defun firefox-update-refresh-firefox ()
  "Send the Control R key to the selected window.
Send the key sequence to `firefox-update-selected-window' window ID."
  (interactive)
  (start-process "refresh-firefox" "refresh-firefox" 
                 "xdotool"
                 ;; "search" "--name" "Mozilla Firefox" "key" "--clearmodifiers" "CTRL+R"))
                 "key" "--window" firefox-update-selected-window "--clearmodifiers" "CTRL+R"))
;; firefox-update-refresh-firefox:1 ends here

;; [[file:firefox-update.org::*Set a Global Key][Set a Global Key:1]]
(global-set-key (kbd "<f7>") #'firefox-update-refresh-firefox)
;; Set a Global Key:1 ends here
