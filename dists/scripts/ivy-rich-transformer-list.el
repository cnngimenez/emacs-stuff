;;; ivy-rich-transformer-list.el --- One line description  -*- lexical-binding: t; -*-

;; Copyright 2023 Christian Gimenez
;;
;; Author: Christian Gimenez
;; Maintainer: Christian Gimenez
;; Version: 0.1.0
;; Keywords: 
;; URL:
;; Package-Requires: ((emacs "27.1"))

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.


(setq ivy-rich-display-transformers-list
      '(ivy-switch-buffer
        (:columns
         ((all-the-icons-ivy-rich-buffer-icon)
          (ivy-rich-candidate
           (:width 30))
          (ivy-rich-switch-buffer-size
           (:width 7))
          (ivy-rich-switch-buffer-indicators
           (:width 4 :face error :align right))
          (ivy-rich-switch-buffer-major-mode
           (:width 12 :face warning)))
         :predicate
         (lambda
           (cand)
           (get-buffer cand))
         :delimiter "	")
        ivy-switch-buffer-other-window
        (:columns
         ((all-the-icons-ivy-rich-buffer-icon)
          (ivy-rich-candidate
           (:width 30))
          (ivy-rich-switch-buffer-size
           (:width 7))
          (ivy-rich-switch-buffer-indicators
           (:width 4 :face error :align right))
          (ivy-rich-switch-buffer-major-mode
           (:width 12 :face warning)))
         :predicate
         (lambda
           (cand)
           (get-buffer cand))
         :delimiter "	")
        counsel-switch-buffer
        (:columns
         ((all-the-icons-ivy-rich-buffer-icon)
          (ivy-rich-candidate
           (:width 30))
          (ivy-rich-switch-buffer-size
           (:width 7))
          (ivy-rich-switch-buffer-indicators
           (:width 4 :face error :align right))
          (ivy-rich-switch-buffer-major-mode
           (:width 12 :face warning)))
         :predicate
         (lambda
           (cand)
           (get-buffer cand))
         :delimiter "	")
        counsel-switch-buffer-other-window
        (:columns
         ((all-the-icons-ivy-rich-buffer-icon)
          (ivy-rich-candidate
           (:width 30))
          (ivy-rich-switch-buffer-size
           (:width 7))
          (ivy-rich-switch-buffer-indicators
           (:width 4 :face error :align right))
          (ivy-rich-switch-buffer-major-mode
           (:width 12 :face warning)))
         :predicate
         (lambda
           (cand)
           (get-buffer cand))
         :delimiter "	")
        counsel-M-x
        (:columns
         ((all-the-icons-ivy-rich-function-icon)
          (counsel-M-x-transformer
           (:width 40))
          (ivy-rich-counsel-function-docstring
           (:face font-lock-doc-face))))
        counsel-describe-function
        (:columns
         ((all-the-icons-ivy-rich-function-icon)
          (counsel-describe-function-transformer
           (:width 40))
          (ivy-rich-counsel-function-docstring
           (:face font-lock-doc-face))))
        counsel-describe-variable
        (:columns
         ((all-the-icons-ivy-rich-variable-icon)
          (counsel-describe-variable-transformer
           (:width 40))
          (ivy-rich-counsel-variable-docstring
           (:face font-lock-doc-face))))
        counsel-describe-symbol
        (:columns
         ((all-the-icons-ivy-rich-symbol-icon)
          (ivy-rich-candidate))
         :delimiter "	")
        counsel-set-variable
        (:columns
         ((all-the-icons-ivy-rich-variable-icon)
          (counsel-describe-variable-transformer
           (:width 40))
          (ivy-rich-counsel-variable-docstring
           (:face font-lock-doc-face))))
        counsel-apropos
        (:columns
         ((all-the-icons-ivy-rich-symbol-icon)
          (ivy-rich-candidate))
         :delimiter "	")
        counsel-info-lookup-symbol
        (:columns
         ((all-the-icons-ivy-rich-symbol-icon)
          (ivy-rich-candidate))
         :delimiter "	")
        counsel-descbinds
        (:columns
         ((all-the-icons-ivy-rich-keybinding-icon)
          (ivy-rich-candidate))
         :delimiter "	")
        counsel-find-file
        (:columns
         ((all-the-icons-ivy-rich-file-icon)
          (ivy-read-file-transformer))
         :delimiter "	")
        counsel-file-jump
        (:columns
         ((all-the-icons-ivy-rich-file-icon)
          (ivy-rich-candidate))
         :delimiter "	")
        counsel-dired
        (:columns
         ((all-the-icons-ivy-rich-file-icon)
          (ivy-read-file-transformer))
         :delimiter "	")
        counsel-dired-jump
        (:columns
         ((all-the-icons-ivy-rich-file-icon)
          (ivy-rich-candidate))
         :delimiter "	")
        counsel-el
        (:columns
         ((all-the-icons-ivy-rich-symbol-icon)
          (ivy-rich-candidate))
         :delimiter "	")
        counsel-fzf
        (:columns
         ((all-the-icons-ivy-rich-file-icon)
          (ivy-rich-candidate))
         :delimiter "	")
        counsel-git
        (:columns
         ((all-the-icons-ivy-rich-file-icon)
          (ivy-rich-candidate))
         :delimiter "	")
        counsel-recentf
        (:columns
         ((all-the-icons-ivy-rich-file-icon)
          (ivy-rich-candidate
           (:width 0.8))
          (ivy-rich-file-last-modified-time
           (:face font-lock-comment-face)))
         :delimiter "	")
        counsel-buffer-or-recentf
        (:columns
         ((all-the-icons-ivy-rich-file-icon)
          (counsel-buffer-or-recentf-transformer
           (:width 0.8))
          (ivy-rich-file-last-modified-time
           (:face font-lock-comment-face)))
         :delimiter "	")
        counsel-bookmark
        (:columns
         ((all-the-icons-ivy-rich-bookmark-type)
          (all-the-icons-ivy-rich-bookmark-name
           (:width 40))
          (all-the-icons-ivy-rich-bookmark-info))
         :delimiter "	")
        counsel-bookmarked-directory
        (:columns
         ((all-the-icons-ivy-rich-file-icon)
          (ivy-rich-candidate))
         :delimiter "	")
        counsel-package
        (:columns
         ((all-the-icons-ivy-rich-package-icon)
          (ivy-rich-candidate
           (:width 30))
          (all-the-icons-ivy-rich-package-version
           (:width 16 :face font-lock-comment-face))
          (all-the-icons-ivy-rich-package-archive-summary
           (:width 7 :face font-lock-builtin-face))
          (all-the-icons-ivy-rich-package-install-summary
           (:face font-lock-doc-face)))
         :delimiter "	")
        counsel-fonts
        (:columns
         ((all-the-icons-ivy-rich-font-icon)
          (ivy-rich-candidate))
         :delimiter "	")
        counsel-major
        (:columns
         ((all-the-icons-ivy-rich-mode-icon)
          (ivy-rich-candidate))
         :delimiter "	")
        counsel-find-library
        (:columns
         ((all-the-icons-ivy-rich-library-icon)
          (ivy-rich-candidate))
         :delimiter "	")
        counsel-load-library
        (:columns
         ((all-the-icons-ivy-rich-library-icon)
          (ivy-rich-candidate))
         :delimiter "	")
        counsel-load-theme
        (:columns
         ((all-the-icons-ivy-rich-theme-icon)
          (ivy-rich-candidate))
         :delimiter "	")
        counsel-world-clock
        (:columns
         ((all-the-icons-ivy-rich-world-clock-icon)
          (ivy-rich-candidate))
         :delimiter "	")
        counsel-tramp
        (:columns
         ((all-the-icons-ivy-rich-tramp-icon)
          (ivy-rich-candidate))
         :delimiter "	")
        counsel-git-checkout
        (:columns
         ((all-the-icons-ivy-rich-git-branch-icon)
          (ivy-rich-candidate))
         :delimiter "	")
        counsel-list-processes
        (:columns
         ((all-the-icons-ivy-rich-process-icon)
          (ivy-rich-candidate))
         :delimiter "	")
        counsel-projectile-switch-project
        (:columns
         ((all-the-icons-ivy-rich-file-icon)
          (ivy-rich-candidate))
         :delimiter "	")
        counsel-projectile-find-file
        (:columns
         ((all-the-icons-ivy-rich-file-icon)
          (counsel-projectile-find-file-transformer))
         :delimiter "	")
        counsel-projectile-find-dir
        (:columns
         ((all-the-icons-ivy-rich-project-icon)
          (counsel-projectile-find-dir-transformer))
         :delimiter "	")
        counsel-minor
        (:columns
         ((all-the-icons-ivy-rich-mode-icon)
          (ivy-rich-candidate))
         :delimiter "	")
        counsel-imenu
        (:columns
         ((all-the-icons-ivy-rich-imenu-icon)
          (ivy-rich-candidate))
         :delimiter "	")
        counsel-cd
        (:columns
         ((all-the-icons-ivy-rich-file-icon)
          (ivy-rich-candidate))
         :delimiter "	")
        counsel-company
        (:columns
         ((all-the-icons-ivy-rich-company-icon)
          (ivy-rich-candidate))
         :delimiter "	")
        package-install
        (:columns
         ((all-the-icons-ivy-rich-package-icon)
          (ivy-rich-candidate
           (:width 30))
          (ivy-rich-package-version
           (:width 16 :face font-lock-comment-face))
          (ivy-rich-package-archive-summary
           (:width 7 :face font-lock-builtin-face))
          (ivy-rich-package-install-summary
           (:face font-lock-doc-face)))
         :delimiter "	")
        package-reinstall
        (:columns
         ((all-the-icons-ivy-rich-package-icon)
          (ivy-rich-candidate
           (:width 30))
          (ivy-rich-package-version
           (:width 16 :face font-lock-comment-face))
          (ivy-rich-package-archive-summary
           (:width 7 :face font-lock-builtin-face))
          (ivy-rich-package-install-summary
           (:face font-lock-doc-face)))
         :delimiter "	")
        package-delete
        (:columns
         ((all-the-icons-ivy-rich-package-icon)
          (ivy-rich-candidate))
         :delimiter "	")
        persp-switch-to-buffer
        (:columns
         ((all-the-icons-ivy-rich-buffer-icon)
          (ivy-rich-candidate
           (:width 30))
          (ivy-rich-switch-buffer-size
           (:width 7))
          (ivy-rich-switch-buffer-indicators
           (:width 4 :face error :align right))
          (ivy-rich-switch-buffer-major-mode
           (:width 12 :face warning)))
         :predicate
         (lambda
           (cand)
           (get-buffer cand))
         :delimiter "	")
        persp-switch
        (:columns
         ((all-the-icons-ivy-rich-project-icon)
          (ivy-rich-candidate))
         :delimiter "	")
        persp-frame-switch
        (:columns
         ((all-the-icons-ivy-rich-project-icon)
          (ivy-rich-candidate))
         :delimiter "	")
        persp-window-switch
        (:columns
         ((all-the-icons-ivy-rich-project-icon)
          (ivy-rich-candidate))
         :delimiter "	")
        persp-kill
        (:columns
         ((all-the-icons-ivy-rich-project-icon)
          (ivy-rich-candidate))
         :delimiter "	")
        persp-save-and-kill
        (:columns
         ((all-the-icons-ivy-rich-project-icon)
          (ivy-rich-candidate))
         :delimiter "	")
        persp-import-buffers
        (:columns
         ((all-the-icons-ivy-rich-project-icon)
          (ivy-rich-candidate))
         :delimiter "	")
        persp-import-win-conf
        (:columns
         ((all-the-icons-ivy-rich-project-icon)
          (ivy-rich-candidate))
         :delimiter "	")
        persp-kill-buffer
        (:columns
         ((all-the-icons-ivy-rich-buffer-icon)
          (ivy-rich-candidate))
         :delimiter "	")
        persp-remove-buffer
        (:columns
         ((all-the-icons-ivy-rich-buffer-icon)
          (ivy-rich-candidate))
         :delimiter "	")
        persp-add-buffer
        (:columns
         ((all-the-icons-ivy-rich-buffer-icon)
          (ivy-rich-candidate))
         :delimiter "	")
        all-the-icons-ivy-rich-kill-buffer
        (:columns
         ((all-the-icons-ivy-rich-buffer-icon)
          (ivy-rich-candidate))
         :delimiter "	")
        treemacs-projectile
        (:columns
         ((all-the-icons-ivy-rich-file-icon)
          (ivy-rich-candidate))
         :delimiter "	")))

;;; ivy-rich-transformer-list.el ends here
