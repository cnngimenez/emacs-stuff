;;; my-secrets.el --- Secrets storage  -*- lexical-binding: t; -*-

;; Copyright 2022 Christian Gimenez
;;
;; Author: Christian Gimenez
;; Maintainer: Christian Gimenez
;; Version: 0.1.0
;; Keywords: convenience
;; URL:
;; Package-Requires: ((emacs "27.1"))

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.


;;; Commentary:

;; A very very very very very simple implementation of a secret
;; storage.
;;
;; The storage is a simple alist that is loaded into a variable.  It
;; is recommended to use a GnuPG AES encrypted file.  Emacs loads
;; them transparently, asking for password when needed.
;;
;; For instance, the secrets-data.el.gpg file may have the following:
;; (("example.org-favorite-film" . "The Pink Pather")
;;  ("example.org-login-username" . "HMancini"))
;;
;; To load this information, use `my-secrets-load'.  Then use `my-secrets-get'
;; as follows: (my-secrets-get "example.org-favourite-film").  The return
;; value would be the string "The Pink Panther".
;;
;; Also, it is possible to store any other ELisp expression in the right
;; side of the cons.  For example:
;; (("example.org-login-username" . "HMancini")
;;  ("example.org-my-age" . (or (im-not-going-to-tell-you!) 50))
;;  ("example.org-my-s-expression" . '(progn (doctor) (butterfly))))
;;

;;; Code:

(provide 'my-secrets)

(defgroup my-secrets nil
  "My-Secrets."
  :group 'applications)
  
(defcustom my-secrets-filepath "~/emacs-stuff/secrets/secrets-data.el.gpg"
  "The secrets file."
  :type 'file
  :group 'my-secrets)

(defvar my-secrets-storage-loaded nil
  "Were the secret file loaded?")

(defvar my-secrets-data nil
  "The secrets data loaded from the file.")

(defun my-secrets-load (&optional force-load)
  "Load the secrets file if it was not loaded before.
FORCE-LOAD is optional, by default to nil.  Load all secrets again if
FORCE-LOAD is t."
  (when (and (or force-load (not my-secrets-storage-loaded))
             (file-exists-p my-secrets-filepath))
    
    (setq my-secrets-storage-loaded t)
    
    (with-temp-buffer
      (insert-file-contents my-secrets-filepath)
      (setq my-secrets-data (read (buffer-string))))))

(defun my-secrets-save ()
  "Save secrets in `my-secrets-data' into the `my-secrets-filepath'."
  (with-temp-buffer
    (insert (prin1-to-string my-secrets-data))
    (write-file my-secrets-filepath)))
  
(defun my-secrets-get (name)
  "Search for a secret named NAME.
If the secrets storage was not loaded, load it."
  (unless my-secrets-storage-loaded
    (my-secrets-load))

  (let ((ret (cdr (assoc name my-secrets-data))))
    (when (not ret)
      (message "Secrets: Requested %S secret, and it is not assigned: nil returned."
               name))
    ret))
 

;;; my-secrets.el ends here
