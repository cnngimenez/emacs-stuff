;;; my-worldtime.el --- World time tables and functions  -*- lexical-binding: t; -*-

;; Copyright 2024 Christian Gimenez
;;
;; Author: Christian Gimenez
;; Maintainer: Christian Gimenez
;; Version: 0.1.0
;; Keywords: 
;; URL:
;; Package-Requires: ((emacs "27.1"))

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.


;;; Commentary:

;; 

;;; Code:

(require 'time)

(defconst my-wt-regexp "^[^#[:space:]]+[[:space:]]+[^[:space:]]+[[:space:]]+\\([^[:space:]]+\\)"
  "Regexp to extract TZ column from .tab files.")

(defvar my-wt-tab-file "/usr/share/zoneinfo/zone1970.tab"
  "The .tab file to extract the TZ.")

(defvar my-wt-timezone-cache nil
  "The timezone list cache.
It is updated when first called `my-wt-timezone-list' or with
`my-wt-timezone-update-cache'.")

(defun my-wt-timezone-list ()
  "Return a timezone list from the .tab file."
  (unless my-wt-timezone-cache
    (my-wt-timezone-update-cache))
  my-wt-timezone-cache)
(defun my-wt-timezone-update-cache ()
  "Update `my-wt-timezone-cache' with the list of timezones.
Return the newly generated cache that has just been stored on `my-wt-timezone-cache'."
  (setq my-wt-timezone-cache nil)
  (with-temp-buffer
    (insert-file-contents-literally my-wt-tab-file)
    (goto-char (point-min))
    (while (search-forward-regexp my-wt-regexp nil t)
      (push (match-string-no-properties 1) my-wt-timezone-cache)))
  my-wt-timezone-cache)

(defun my-wt-add-timezone-to-world-clock (timezone label)
  "Add a timezone entry to `world-clock-list'.
Add a new entry to `world-clock-list'.  Autocomplete is supported via
`completing-read'.

TIMEZONE is a string with the timezone as appears in the .tab file.
LABEL is a string with the user preferred label for this timezone."
  (interactive
   (list (completing-read "Timezone?" (my-wt-timezone-list) nil t)
         (read-string "Label?")))
  
  (when (and world-clock-list
             (not (listp world-clock-list)))
    ;; The `world-clock-list' is the t value.
    (setq world-clock-list nil))
  
  (push (cons timezone label) world-clock-list)
  world-clock-list)
   

(provide 'my-worldtime)
;;; my-worldtime.el ends here
