(require 'org)

(defun org-geminilinks-switch-link-follow (url &optional arg)
    "Open a Gemini Link by using `browse-url' properly.
This function is intended for `org-link-parameters' and
`org-link-set-parameters'.  Use `browse-url-at-point' or `browse-url' to open
a gemini URL with elpher or other browser."
    (browse-url (concat "gemini:" url) arg))

(org-link-set-parameters "gemini" :follow #'org-geminilinks-switch-link-follow)

(provide 'org-geminilinks)
