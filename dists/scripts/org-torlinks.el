(require 'org)

(defgroup org-torlinks nil "Tor link support for Org-link."
  :group 'org-link)

(defcustom org-torlinks-program-path "~/Downloads/tor-browser_en-US/"
  "Where is the Tor dekstop file used to launch it."
  :group 'org-torlinks
  :type 'directory)

(defun org-torlinks-program-exec-path (&optional dirpath)
  "Return the path of the executable used by Tor."
  
  (concat (if dirpath
              dirpath
            org-torlinks-program-path)
          "/Browser/start-tor-browser"))

(defun org-torlinks-switch-link-follow (url &rest _)
  (interactive "MURL?")
  (let ((execpath  (org-torlinks-program-exec-path org-torlinks-program-path))
        (basharg (format "date;cd %s;./Browser/start-tor-browser https:%s"
                               org-torlinks-program-path
                               url)))
    (if (file-executable-p execpath)
        (progn 
          (message "Running %s" basharg)
          (call-process "bash"
                        nil nil nil
                         "-c" basharg))
      (message "Tor program not found: %s" execpath))))

(org-link-set-parameters "tor-https" :follow #'org-torlinks-switch-link-follow)

(provide 'org-torlinks)
