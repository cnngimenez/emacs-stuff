;;; radio.el --- Listen to Internet transmissions  -*- lexical-binding: t; -*-

;; Copyright 2024 Christian Gimenez
;;
;; Author: Christian Gimenez
;; Maintainer: Christian Gimenez
;; Version: 0.1.0
;; Keywords: tools, convenience
;; URL: https://gitlab.com/cnngimenez/emacs-stuff
;; Package-Requires: ((emacs "27.1"))

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.


;;; Commentary:
;;
;; Store radio configurations and play them.
;;

;;; Code:

(defvar radio-process nil
  "The radio process.")

(defcustom radio-commands nil
  "Commands to connect to radio.
Example value:
  ((\"my-radio1\" . (url . \"https://my-page/\"))
   (\"my-radio2\" . (url . \"https://other-radio/\")))"
  :type '(alist :key-type string
                :value-type (alist :key-type symbol
                                   :value-type string))
  :group 'radio)

(defun radio-status ()
  "Show the status of the radio.
Is the radio playing or not?"
  (interactive)
  (if (and (not (null radio-process))
           (equal (process-status radio-process) 'run))
      (message "Radio is playing.")
    (message "Radio is stopped.")))

(defun radio-stop ()
  "Stop the radio audio."
  (interactive)
  (when (and (not (null radio-process))
             (equal (process-status radio-process) 'run))
    (delete-process radio-process))
  (setq radio-process nil))

(defun radio-listen (name)
  "Start listening to a configured radio.
NAME is the radio configuration name.  See `radio-commands'."
  (interactive (list
                (completing-read "Radio?" (mapcar #'car radio-commands))))
  (radio-stop)
  (let* ((radio-data (cdr (assoc name radio-commands)))
         (radio-url (cdr (assoc 'url radio-data))))
    (setq radio-process (start-process "radio-process" "*radio-process*"
                                       "mpv" "--quiet" radio-url))))
        
(provide 'radio)
;;; radio.el ends here
