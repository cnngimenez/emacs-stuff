;;; unit-conversions.el --- Convert between units  -*- lexical-binding: t; -*-

;; Copyright 2024 Christian Gimenez
;;
;; Author: Christian Gimenez
;; Maintainer: Christian Gimenez
;; Version: 0.1.0
;; Keywords: convenience
;; URL: https://gitlab.com/cnngimenez/emacs-stuff
;; Package-Requires: ((emacs "27.1"))

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.


;;; Commentary:

;; Functions to convert between length and other unit measures.

;;; Code:

;; [[file:unit-conversions.org::*uc-length-equivalence][uc-length-equivalence:1]]
(defconst uc-length-equivalence '((in . 1.0)
                           (cm . 2.54)
                           (pt . 72.0)
                           (pc . 6.0))
  "Equivalences between 1 inch and other unit measures.")
;; uc-length-equivalence:1 ends here

;; [[file:unit-conversions.org::*uc-read-unit][uc-read-unit:1]]
(defun uc-read-unit (prompt)
  "Read from user with completion the unit.
PROMPT is a string with the prompt message for the user."
  (intern
   (completing-read prompt (mapcar #'car uc-length-equivalence))))
;; uc-read-unit:1 ends here

;; [[file:unit-conversions.org::*uc-convert][uc-convert:1]]
(defun uc-convert (amount from to)
  "Convert an amount between one unit measure to another.
FROM is a symbol with the unit name of AMOUNT (ex.: \\='cm, \\='in).
TO is as symbol with the unit name to convert.

For instance, to convert 25 cm into inches, use the following code:
  (uc-convert 25 \\='cm \\='in)"
  (interactive (list (read-number "Enter amount?")
                     (uc-read-unit "Amount unit?")
                     (uc-read-unit "Convert to?")))

  (let* ((from-unit (cdr (assoc from uc-length-equivalence)))
         (to-unit   (cdr (assoc to uc-length-equivalence)))
         (results   (/ (* to-unit amount) from-unit)))
    (when (called-interactively-p 'any)
      (message "%s %s = %s %s" amount from results to))
    results))
;; uc-convert:1 ends here

;; [[file:unit-conversions.org::*uc-convert-to-px][uc-convert-to-px:1]]
(defun uc-convert-to-px (from-amount from-unit &optional dpi)
  "Convert a measure into pixels.
Depending on the dots per inch (dpi), convert the given amount from the given
unit into pixels.

Parameters are:
FROM-AMOUNT the number of the measure.
FROM-UNIT is the symbol with the unit name.
DPI is the dots per inch value which is optional.

The common DPI values are 96, 100, 220, 1800.  This value depends on the
screen.  If not provided 96 is used."
  (interactive (list (read-number "Enter amount?")
                     (uc-read-unit  "Amount unit?")
                     (read-number "DPI?" 96.0)))

  (let* ((from-inches (if (equal from-unit 'in)
                          from-amount
                        (uc-convert from-amount from-unit 'in)))
         (dpi-num (or dpi 96.0))
         (results (* dpi-num from-inches)))
    (when (called-interactively-p 'any)
      (message "%s %s = %s px (with %s dpi)" from-amount from-unit results dpi))

    results))
;; uc-convert-to-px:1 ends here

;; [[file:unit-conversions.org::*uc-convert-from-px][uc-convert-from-px:1]]
(defun uc-convert-from-px (px-amount to-unit &optional dpi)
  "Convert from px to the given measure unit.
PX-AMOUNT is the number of pixels to convert.
TO-UNIT is the symbol with the unit name.
DPI is the number of dots per inchs.

Usual DPI values are 96, 100, 220, and 1800, but it depends on the screen.
The default is 96."
  (interactive (list (read-number "Pixels amount?")
                     (uc-read-unit "Convert to?")
                     (read-number "DPI?" 96.0)))

  (let* ((dpi-num (or dpi 96.0))
         (px-unit (/ 1.0 dpi-num))
         (results (if (equal to-unit 'in)
                      (* px-unit px-amount)
                    (uc-convert (* px-unit px-amount) 'in to-unit))))

    (when (called-interactively-p 'any)
      (message "%s px = %s %s (with %s dpi)" px-amount results to-unit dpi))

    results))
;; uc-convert-from-px:1 ends here

(provide 'unit-conversions)
;;; unit-conversions.el ends here
