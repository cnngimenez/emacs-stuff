;; [[file:eshell.org::*ll][ll:1]]
(defun eshell/ll (&rest args)
  "A particular `eshell/ls' call with -lh parameters."
  (eshell/ls (append '("-lh") args)))
;; ll:1 ends here

;; [[file:eshell.org::*fun][fun:1]]
(defun eshell/fun (&rest args)
  "Run a fish shell function."
  (eshell-command (mapconcat (lambda (x) (format "%s " x)) args)))
  ;; (eshell-command "fish -c "))
;; fun:1 ends here
