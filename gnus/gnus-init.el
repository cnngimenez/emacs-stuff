;; [[file:../init.org::*Support for Atom][Support for Atom:1]]
;; This will be the default .gnus.el. We change its location.

;; Usar Atom
(require 'mm-url)
(defadvice mm-url-insert (after DE-convert-atom-to-rss () )
  "Converts atom to RSS by calling xsltproc."
  (when (re-search-forward "xmlns=\"http://www.w3.org/.*/Atom\"" 
                           nil t)
    (goto-char (point-min))
    (message "Converting Atom to RSS... ")
    (call-process-region (point-min) (point-max) 
                         "xsltproc" 
                         t t nil 
                         (expand-file-name
                          "~/emacs-stuff/gnus/atom2rss.xsl") "-")
    (goto-char (point-min))
    (message "Converting Atom to RSS... done")))

(ad-activate 'mm-url-insert)

;; Para que no inicie de buenas a primeras cargando
(setq gnus-select-method '(nnrss ""))
;; Support for Atom:1 ends here

;; [[file:../init.org::*Load Gnus my-secrets][Load Gnus my-secrets:1]]
(load-file (emacsstuff-dir "secrets/gnus/gnus-init.el"))
;; Load Gnus my-secrets:1 ends here
