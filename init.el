;; [[file:init.org::*Init customisation group][Init customisation group:1]]
(defgroup my-init nil
  "My emacs-stuff init."
  :group 'convenience)
;; Init customisation group:1 ends here

;; [[file:init.org::*Measure init time][Measure init time:1]]
(defvar my-init-start-time (time-to-seconds)
  "The time when the init.org load starts.")
(defvar my-init-stop-time nil
  "The time when the init.org load ends")
;; Measure init time:1 ends here

;; [[file:init.org::*Requirements][Requirements:1]]
(require 'seq)
;; Requirements:1 ends here

;; [[file:init.org::*am-i-on-cellphone-p][am-i-on-cellphone-p:1]]
(defun am-i-on-cellphone-p ()
  "True if this Emacs instance is on a cellphone"
  (numberp (string-search "android" system-configuration)))
;; am-i-on-cellphone-p:1 ends here

;; [[file:init.org::*am-i-on-win-p][am-i-on-win-p:1]]
(defun am-i-on-win-p ()
  "True if this Emacs instance is on a Win OS"
  (or (equal 'windows-nt system-type)
      (numberp (string-search "x86_64-w64-mingw32" system-configuration))))
;; am-i-on-win-p:1 ends here

;; [[file:init.org::*emacsstuff-path][emacsstuff-path:1]]
(defvar emacsstuff-path "~/emacs-stuff/"
  "Path where is the emacs_stuff directory. Add a \"/\" at the end!")
;; emacsstuff-path:1 ends here

;; [[file:init.org::*emacsstuff-dir][emacsstuff-dir:1]]
(defun emacsstuff-dir (dir)
  "Return a string with the complete path of a diectory DIR inside the 
`emacsstuff-path'.
DIR must be a string without initial \"/\".

Example:
  (emacsstuff-dir \"dists/scripts/my-scritp.el\")
"
  (concat emacsstuff-path dir))
;; emacsstuff-dir:1 ends here

;; [[file:init.org::*emacsstuff-packasubpath][emacsstuff-packasubpath:1]]
(defvar emacsstuff-packsubpath "dists/packs/"
  "Where is the packs dir located inside the `emacsstuff-path'.

See `emacsstuff-packsdir' and `emacsstuff-dir'.")
;; emacsstuff-packasubpath:1 ends here

;; [[file:init.org::*emacsstuff-packsdir][emacsstuff-packsdir:1]]
(defun emacsstuff-packsdir (pack-path)
  "Return the complete path of the PACK-PATH.

Use `emacsstuff-packsubpath' variable for changing the packs subdir."
  (concat (emacsstuff-dir emacsstuff-packsubpath) pack-path))
;; emacsstuff-packsdir:1 ends here

;; [[file:init.org::*Add emacsstuff-dirs][Add emacsstuff-dirs:1]]
(add-to-list 'load-path (emacsstuff-dir "dists/scripts"))
(add-to-list 'load-path (emacsstuff-dir "personal"))
(add-to-list 'load-path (emacsstuff-dir "dists/linked-scripts"))
;; Add emacsstuff-dirs:1 ends here

;; [[file:init.org::*Add system site-lisp path][Add system site-lisp path:1]]
(push "/usr/share/emacs/site-lisp" load-path)
;; Add system site-lisp path:1 ends here

;; [[file:init.org::*Username][Username:1]]
(setq user-login-name "")
;; Username:1 ends here

;; [[file:init.org::*my-repo-path][my-repo-path:1]]
(defvar my-repo-path "~/repos/emacs"
  "Where is my Emacs repositories?
The init.org file will load every path here!")
;; my-repo-path:1 ends here

;; [[file:init.org::*my-find-repo-paths][my-find-repo-paths:1]]
(defun my-find-repo-paths ()
  "Return a list of repository directories
Return a list of repositories in `my-repo-path'.  The repo path is
not checked and an error will be raised if it does not exists."
  (mapcar (lambda (x)
            (concat my-repo-path "/" x))
          (seq-difference (directory-files my-repo-path)
                          '("." ".." "emacs"))))
;; my-find-repo-paths:1 ends here

;; [[file:init.org::*my-update-repo-paths][my-update-repo-paths:1]]
(defun my-update-repo-paths ()
  "Update `load-path' with new repositories paths.
Repositories paths should be ~/repos/emacs/* directories."
  (interactive)
  (let ((not-in-load-path (seq-filter (lambda (path)
                                        (not (member path load-path)))
                                        (my-find-repo-paths))))
    (setq load-path (append not-in-load-path load-path))
    (message "Added %s new paths: %s" (length not-in-load-path) not-in-load-path)))
;; my-update-repo-paths:1 ends here

;; [[file:init.org::*Add repos to load-path][Add repos to load-path:1]]
(if (file-exists-p my-repo-path)
    (setq load-path (append load-path (my-find-repo-paths)))
  (progn
    (message "Repository path does not exist! Creating it.")
    (make-directory my-repo-path t)))
;; Add repos to load-path:1 ends here

;; [[file:init.org::*Load my-secrets.el][Load my-secrets.el:1]]
(require 'my-secrets)
;; Load my-secrets.el:1 ends here

;; [[file:init.org::*use-package][use-package:1]]
;; This is only needed once, near the top of the file
(eval-when-compile (require 'use-package))
;; use-package:1 ends here

;; [[file:init.org::*use-package][use-package:2]]
(setq use-package-compute-statistics t)
;; use-package:2 ends here

;; [[file:init.org::*use-package][use-package:3]]
(require 'use-package-ensure)
(setq use-package-always-ensure t)
;; use-package:3 ends here

;; [[file:init.org::*vc-use-package][vc-use-package:1]]
(unless (package-installed-p 'vc-use-package)
  (package-vc-install "https://github.com/slotThe/vc-use-package"))
(require 'vc-use-package)
;; vc-use-package:1 ends here

;; [[file:init.org::*auth-info-password returns a function][auth-info-password returns a function:2]]
(require 'auth-source)

(defun my-auth-info-password (result)
  (if (functionp result)
      (funcall result)
    result))

(advice-add 'auth-info-password :filter-return #'my-auth-info-password)
;; auth-info-password returns a function:2 ends here

;; [[file:init.org::*Convenience and Appearance][Convenience and Appearance:2]]
(delete-selection-mode 1)
(setq inverse-video (not (am-i-on-cellphone-p))
      indent-tabs-mode nil)
;; Convenience and Appearance:2 ends here

;; [[file:init.org::*Disable beep/bell][Disable beep/bell:1]]
(setq visible-bell t)
;; Disable beep/bell:1 ends here

;; [[file:init.org::*Menu and tool bar][Menu and tool bar:1]]
;; (let ((font-family (face-attribute 'default :family))
;;       (font-height (face-attribute 'default :height)))
;;   (menu-bar-mode -1)  ;; Managed by Emacs-Spellbook
;;   (tool-bar-mode -1) ;; Manager by Emacs-spellbook
;;   (set-face-attribute 'default nil
;;                       :font font-family
;;                       :height font-height))

(defun menu-bar-open (&optional frame)
  "Show the menu bar as a text menu.
FRAME is unused."
  (interactive)
  (tmm-menubar))

(global-set-key [f10] 'menu-bar-open)
;; Menu and tool bar:1 ends here

;; [[file:init.org::*Prettify - ligatures][Prettify - ligatures:1]]
;; (message"==> Prettify")
(global-prettify-symbols-mode 1)
;; Prettify - ligatures:1 ends here

;; [[file:init.org::*Check if FiraCode is installed][Check if FiraCode is installed:1]]
(unless (member "FiraCode" (font-family-list))
  (message "Warning: Fira code is needed for ligatures and it was not found!"))
;; Check if FiraCode is installed:1 ends here

;; [[file:init.org::*A working implementation][A working implementation:1]]
(unless (am-i-on-cellphone-p)
  (add-hook 'after-make-frame-functions
            (lambda (frame) (set-fontset-font t
                                              '(#Xe100 . #Xe16f)
                                              "Fira Code Symbol")))
  (set-fontset-font t '(#Xe100 . #Xe16f) "Fira Code Symbol"))

  (defconst fira-code-font-lock-keywords-alist
    (mapcar (lambda (regex-char-pair)
              `(,(car regex-char-pair)
                (0 (prog1 ()
                     (compose-region (match-beginning 1)
                                     (match-end 1)
                                     ,(concat "	"
                                              (list
                                               (decode-char 'ucs
                                                            (cadr regex-char-pair)))))))))

            '(("\\(www\\)"                   #Xe100)
              ("[^/]\\(\\*\\*\\)[^/]"        #Xe101)
              ("\\(\\*\\*\\*\\)"             #Xe102)
              ("\\(\\*\\*/\\)"               #Xe103)
              ("\\(\\*>\\)"                  #Xe104)
              ("[^*]\\(\\*/\\)"              #Xe105)
              ("\\(\\\\\\\\\\)"              #Xe106)
              ("\\(\\\\\\\\\\\\\\)"          #Xe107)
              ("\\({-\\)"                    #Xe108)
              ;; box-looking character ([], aka list operator) looks
              ;; too much like unicode missing-glyph icon. also the cons
              ;; operator (::), along with the list ([]), both fuck up
              ;; the alignment of sml and haskell-code far too goddamn
              ;; often to be of any aesthetical use, rather an irritant
              ;; if anything.

              ;; TODO: FIXME: fix these by adding fake spaces to them?

              ;; ("\\(\\[\\]\\)"                #Xe109)
              ;; ("\\(::\\)"                    #Xe10a)
              ("\\(:::\\)"                   #Xe10b)
              ("[^=]\\(:=\\)"                #Xe10c)
              ;; ("\\(!!\\)"                    #Xe10d)
              ;; this should be hooked to sml-mode only
              ("\\(<>\\)"                    #Xe10e)
              ("\\(!=\\)"                    #Xe10e)
              ("\\(!==\\)"                   #Xe10f)
              ("\\(-}\\)"                    #Xe110)
              ("\\(--\\)"                    #Xe111)
              ("\\(---\\)"                   #Xe112)
              ("\\(-->\\)"                   #Xe113)
              ("[^-]\\(->\\)"                #Xe114)
              ("\\(->>\\)"                   #Xe115)
              ("\\(-<\\)"                    #Xe116)
              ("\\(-<<\\)"                   #Xe117)
              ("\\(-~\\)"                    #Xe118)
              ("\\(#{\\)"                    #Xe119)
              ("\\(#\\[\\)"                  #Xe11a)
              ("\\(##\\)"                    #Xe11b)
              ("\\(###\\)"                   #Xe11c)
              ("\\(####\\)"                  #Xe11d)
              ("\\(#(\\)"                    #Xe11e)
              ("\\(#\\?\\)"                  #Xe11f)
              ("\\(#_\\)"                    #Xe120)
              ("\\(#_(\\)"                   #Xe121)
              ("\\(\\.-\\)"                  #Xe122)
              ("\\(\\.=\\)"                  #Xe123)
              ("\\(\\.\\.\\)"                #Xe124)
              ("\\(\\.\\.<\\)"               #Xe125)
              ("\\(\\.\\.\\.\\)"             #Xe126)
              ("\\(\\?=\\)"                  #Xe127)
              ("\\(\\?\\?\\)"                #Xe128)
              ("\\(;;\\)"                    #Xe129)
              ("\\(/\\*\\)"                  #Xe12a)
              ("\\(/\\*\\*\\)"               #Xe12b)
              ("\\(/=\\)"                    #Xe12c)
              ("\\(/==\\)"                   #Xe12d)
              ("\\(/>\\)"                    #Xe12e)
              ("\\(//\\)"                    #Xe12f)
              ("\\(///\\)"                   #Xe130)
              ("\\(&&\\)"                    #Xe131)
              ("\\(||\\)"                    #Xe132)
              ("\\(||=\\)"                   #Xe133)
              ("[^|]\\(|=\\)"                #Xe134)
              ("\\(|>\\)"                    #Xe135)
              ("\\(\\^=\\)"                  #Xe136)
              ("\\(\\$>\\)"                  #Xe137)
              ("\\(\\+\\+\\)"                #Xe138)
              ("\\(\\+\\+\\+\\)"             #Xe139)
              ("\\(\\+>\\)"                  #Xe13a)
              ("\\(=:=\\)"                   #Xe13b)
              ("[^!/]\\(==\\)[^>]"           #Xe13c)
              ("\\(===\\)"                   #Xe13d)
              ("\\(==>\\)"                   #Xe13e)
              ("[^=]\\(=>\\)"                #Xe13f)
              ("\\(=>>\\)"                   #Xe140)
              ("\\(<=\\)"                    #Xe141)
              ("\\(=<<\\)"                   #Xe142)
              ("\\(=/=\\)"                   #Xe143)
              ("\\(>-\\)"                    #Xe144)
              ("\\(>=\\)"                    #Xe145)
              ("\\(>=>\\)"                   #Xe146)
              ("[^-=]\\(>>\\)"               #Xe147)
              ("\\(>>-\\)"                   #Xe148)
              ("\\(>>=\\)"                   #Xe149)
              ("\\(>>>\\)"                   #Xe14a)
              ("\\(<\\*\\)"                  #Xe14b)
              ("\\(<\\*>\\)"                 #Xe14c)
              ("\\(<|\\)"                    #Xe14d)
              ("\\(<|>\\)"                   #Xe14e)
              ("\\(<\\$\\)"                  #Xe14f)
              ("\\(<\\$>\\)"                 #Xe150)
              ("\\(<!--\\)"                  #Xe151)
              ("\\(<-\\)"                    #Xe152)
              ("\\(<--\\)"                   #Xe153)
              ("\\(<->\\)"                   #Xe154)
              ("\\(<\\+\\)"                  #Xe155)
              ("\\(<\\+>\\)"                 #Xe156)
              ("\\(<=\\)"                    #Xe157)
              ("\\(<==\\)"                   #Xe158)
              ("\\(<=>\\)"                   #Xe159)
              ("\\(<=<\\)"                   #Xe15a)
              ;; ("\\(<>\\)"                    #Xe15b)
              ("[^-=]\\(<<\\)"               #Xe15c)
              ("\\(<<-\\)"                   #Xe15d)
              ("\\(<<=\\)"                   #Xe15e)
              ("\\(<<<\\)"                   #Xe15f)
              ("\\(<~\\)"                    #Xe160)
              ("\\(<~~\\)"                   #Xe161)
              ("\\(</\\)"                    #Xe162)
              ("\\(</>\\)"                   #Xe163)
              ("\\(~@\\)"                    #Xe164)
              ("\\(~-\\)"                    #Xe165)
              ("\\(~=\\)"                    #Xe166)
              ("\\(~>\\)"                    #Xe167)
              ("[^<]\\(~~\\)"                #Xe168)
              ("\\(~~>\\)"                   #Xe169)
              ("\\(%%\\)"                    #Xe16a)
              ("[^:=]\\(:\\)[^:=]"           #Xe16c)
              ("[^\\+<>]\\(\\+\\)[^\\+<>]"   #Xe16d)
              ("[^\\*/<>]\\(\\*\\)[^\\*/<>]" #Xe16f)
              )))

  (defun add-fira-code-symbol-keywords ()
    "Add the Fira Code ligatures from Fira Code Symbol to selected keywords."
    (font-lock-add-keywords nil fira-code-font-lock-keywords-alist))

  ;; (provide 'init-fira-code-ligatures)
;; A working implementation:1 ends here

;; [[file:init.org::*PHP symbols][PHP symbols:1]]
(defconst web-code-font-lock-keywords-alist
  (mapcar (lambda (regex-char-pair)
            `(,(car regex-char-pair)
              (0 (prog1 ()
                   (compose-region (match-beginning 1)
                                   (match-end 1)
                                   ,(concat "	"
                                            (list
                                             (decode-char 'ucs
                                                          (cadr regex-char-pair)))))))))


          '(
          ("<\\(\\?php\\)"        #Xe94f)
          ;; ("<?=" . #xe94f )
            ("\\(function\\)"       #x2131)
            ("\\(return\\)"         #x27fc)
            ("\\(for\\) "           #x2200)

            ;; Base Types
            (" \\(int\\) "       #x2124)
            (" \\(float\\) "     #x211d)
            (" \\(str\\) "       #x1d54a)
            ("[^[:alnum:]]\\(True\\)[^[:alnum:]]"      #x1d54b)
            ("[^[:alnum:]]\\(true\\)[^[:alnum:]]"      #x1d54b)
            ("[^[:alnum:]]\\(False\\)[^[:alnum:]]"     #x1d53d)
            ("[^[:alnum:]]\\(false\\)[^[:alnum:]]"     #x1d53d)
            ("\\(::\\)"             #Xe10a)
            )))

(defun add-web-code-symbol-keywords ()
  "Add the Fira Code ligatures from Fira Code Symbol to selected keywords."
  (font-lock-add-keywords nil web-code-font-lock-keywords-alist))
;; PHP symbols:1 ends here

;; [[file:init.org::*Elisp symbols][Elisp symbols:1]]
(defconst elisp-code-font-lock-keywords-alist
  (mapcar (lambda (regex-char-pair)
            `(,(car regex-char-pair)
              (0 (prog1 ()
                   (compose-region (match-beginning 1)
                                   (match-end 1)
                                   ,(concat "	"
                                            (list
                                             (decode-char 'ucs
                                                          (cadr regex-char-pair)))))))))


          '(
            ("\\(defvar\\)"     #x1d4b1) 
            ("\\(defconst\\)"   #x1d49e) ;; otra: #x212d
            ("\\(defun\\)"       #x2131)
            ("\\(dolist\\) "           #x2200)

            ;; Base Types
            (" \\(int\\) "       #x2124)
            (" \\(float\\) "     #x211d)
            (" \\(str\\) "       #x1d54a)
            ("[^[:alnum:]]\\(True\\)[^[:alnum:]]"      #x1d54b)
            ("[^[:alnum:]]\\(true\\)[^[:alnum:]]"      #x1d54b)
            ("[^[:alnum:]]\\(False\\)[^[:alnum:]]"     #x1d53d)
            ("[^[:alnum:]]\\(false\\)[^[:alnum:]]"     #x1d53d)
            )))

(defun add-elisp-code-symbol-keywords ()
  "Add the Fira Code ligatures from Fira Code Symbol to selected keywords."
  (font-lock-add-keywords nil elisp-code-font-lock-keywords-alist))
;; Elisp symbols:1 ends here

;; [[file:init.org::*Adding hooks to affected modes][Adding hooks to affected modes:1]]
(unless (am-i-on-cellphone-p)
  (add-hook 'prog-mode-hook #'add-fira-code-symbol-keywords)
  ;; (add-hook 'scheme-mode-hook  #'add-fira-code-symbol-keywords)
  ;; (add-hook 'clojure-mode-hook #'add-fira-code-symbol-keywords)
  (add-hook 'web-mode-hook  #'add-web-code-symbol-keywords)
  (add-hook 'emacs-lisp-mode-hook #'add-elisp-code-symbol-keywords))
;; Adding hooks to affected modes:1 ends here

;; [[file:init.org::*Auto-fill][Auto-fill:1]]
;; (add-hook 'find-file-hooks 'auto-fill-mode)
(setq fill-column 80)
;; Auto-fill:1 ends here

;; [[file:init.org::*eldoc-mode][eldoc-mode:2]]
(global-eldoc-mode t)
;; eldoc-mode:2 ends here

;; [[file:init.org::*Finding prolog][Finding prolog:1]]
(add-to-list 'load-path "~/repos/emacs/finding-prolog")

(use-package fp
:ensure nil
:defer t)
;; Finding prolog:1 ends here

;; [[file:init.org::*Embark][Embark:1]]
(use-package embark
  :ensure t
  :bind (("C-." . embark-act)
         ("C-;" . embark-dwim)
         ("C-h B" . embark-bindings)))
;; Embark:1 ends here

;; [[file:init.org::*SachaC-news][SachaC-news:1]]
(use-package sachac-news
  :if (locate-library "sachac-news")
  :ensure nil
  :config
  (sachac-news-activate-timer))
;; SachaC-news:1 ends here

;; [[file:init.org::*kdeconnect][kdeconnect:2]]
(if (file-exists-p "~/repos/emacs/kdeconnect.el/kdeconnect.el")
    (load-library "kdeconnect")
  (message "kdeconnect.el couldn't be found. Please clone the repository from https://github.com/cnngimenez/kdeconnect.el.git"))
;; kdeconnect:2 ends here

;; [[file:init.org::*Async shell command won't show the output buffer][Async shell command won't show the output buffer:1]]
(unless (assoc shell-command-buffer-name-async display-buffer-alist)
  (push (cons shell-command-buffer-name-async '((display-buffer-no-window)))
        display-buffer-alist))
;; Async shell command won't show the output buffer:1 ends here

;; [[file:init.org::*Org-mode][Org-mode:1]]
(require 'ox-latex)
;; Org-mode:1 ends here

;; [[file:init.org::*Agenda Icons org-agenda-category-icon-alist][Agenda Icons org-agenda-category-icon-alist:1]]
(unless (am-i-on-cellphone-p)
  (setq org-agenda-category-icon-alist
        `(("computers" ,(list (all-the-icons-material "computer"))
           nil nil :ascent center)
          ("books" ,(list (all-the-icons-faicon "book"))
           nil nil :ascent center)
          ("congresos" ,(list (all-the-icons-faicon "university"))
           nil nil :ascent center)
          ("teaching" ,(list (all-the-icons-faicon "graduation-cap"))
           nil nil :ascent center)
          ("master" ,(list (all-the-icons-faicon "book"))
           nil nil :ascent center)
          ("radio" ,(list (all-the-icons-material "radio"))
           nil nil :ascent center)
          ("home" ,(list (all-the-icons-faicon "home"))
           nil nil :ascent center)
          ("internals" ,(list (all-the-icons-faicon "cog"))
           nil nil :ascent center)
          ("google" ,(list (all-the-icons-faicon "google"))
           nil nil :ascent center)
          ("holidays" ,(list (all-the-icons-faicon "calendar-times-o"))
           nil nil :ascent center)
          ("efemérides" ,(list (all-the-icons-faicon "calendar-times-o"))
           nil nil :ascent center)            
          ("Diary" ,(list (all-the-icons-faicon "calendar-times-o"))
           nil nil :ascent center))))
;; Agenda Icons org-agenda-category-icon-alist:1 ends here

;; [[file:init.org::*Tags position in agenda][Tags position in agenda:1]]
(setq org-agenda-tags-column -80)
;; Tags position in agenda:1 ends here

;; [[file:init.org::*Exporting iCalendar files to Syncthing directory][Exporting iCalendar files to Syncthing directory:1]]
(setq org-icalendar-combined-agenda-file "~/Sync/orgs/org.ics")
;; Exporting iCalendar files to Syncthing directory:1 ends here

;; [[file:init.org::*my-do-not-export-to-ics][my-do-not-export-to-ics:1]]
(defvar my-do-not-export-to-ics '("/home/poo/Sync/orgs/google.org")
  "Exclude these paths from exporting them to the ics file.")
;; my-do-not-export-to-ics:1 ends here

;; [[file:init.org::*my-export-org-agenda-to-ics][my-export-org-agenda-to-ics:1]]
(defun my-export-org-agenda-to-ics (&optional sync)
  "Export the org-agenda files into ics files.
If SYNC is t, then run synchronously."
  (interactive "P")
  (let ((org-export-async-init-file (make-temp-file "org-export-init-file"))
        (org-agenda-files (cl-remove-if (lambda (path)
                                          (member path my-do-not-export-to-ics))
                                        (org-agenda-files t))))
    ;; Add the needed variable to generate the ICS file at the desired location.
    (with-temp-file org-export-async-init-file
      (insert (format "%S" `(setq org-icalendar-combined-agenda-file
                                  ,org-icalendar-combined-agenda-file))))

    ;; (message "Exporting %s files" org-agenda-files)
    (org-icalendar-combine-agenda-files (not sync))))
;; my-export-org-agenda-to-ics:1 ends here

;; [[file:init.org::*Run once per hour][Run once per hour:1]]
(run-at-time t 3600 #'my-export-org-agenda-to-ics)
;; Run once per hour:1 ends here

;; [[file:init.org::*Global Keys][Global Keys:1]]
(global-set-key "\C-cl" 'org-store-link)
(global-set-key "\C-cc" 'org-capture)
(global-set-key "\C-ca" 'org-agenda)
(global-set-key "\C-cb" 'org-iswitchb)
;; Global Keys:1 ends here

;; [[file:init.org::*my-org-copy-begin-src][my-org-copy-begin-src:1]]
(defun my-org-copy-begin-src ()
  "Copy the last begin_src used."
  (interactive)
  (let ((tangle-text nil))
    (save-excursion
      (when (search-backward "#+begin_src" nil t)
        (setq tangle-text
              (buffer-substring-no-properties (line-beginning-position) (line-end-position)))))
    (insert tangle-text)
    (insert "\n\n#+end_src\n")
    (forward-line -2)))
;; my-org-copy-begin-src:1 ends here

;; [[file:init.org::*Set key][Set key:1]]
(define-key org-mode-map (kbd "C-c s") #'my-org-copy-begin-src)
;; Set key:1 ends here

;; [[file:init.org::*Personal Directories][Personal Directories:1]]
(if (file-exists-p "~/Sync/orgs")
    (progn
      (setq org-directory "~/Sync/orgs/")
      (setq org-agenda-files '("~/Sync/orgs/" "~/Sync/orgs/from-orgzly/a-agregar.org")))
  (message "Warning: Org agenda files could not be found at ~/Sync/orgs/"))
;; Personal Directories:1 ends here

;; [[file:init.org::*Archive location][Archive location:1]]
(setq org-archive-location "~/docs/org/archive/%s::")
;; Archive location:1 ends here

;; [[file:init.org::*org-man - Manpage links][org-man - Manpage links:1]]
(use-package org-man
  :ensure nil
  :after org)
;; org-man - Manpage links:1 ends here

;; [[file:init.org::*org-elisp-help - Elisp help links][org-elisp-help - Elisp help links:1]]
(use-package org-elisp-help
  :ensure t
  :after org)
;; org-elisp-help - Elisp help links:1 ends here

;; [[file:init.org::*Tor-https][Tor-https:1]]
(load-library "org-torlinks")
;; Tor-https:1 ends here

;; [[file:init.org::*Gemini URLs][Gemini URLs:1]]
(load-library "org-geminilinks")
;; Gemini URLs:1 ends here

;; [[file:init.org::*ascii-art-to-unicode][ascii-art-to-unicode:1]]
(use-package ascii-art-to-unicode
  :defer t)
;; ascii-art-to-unicode:1 ends here

;; [[file:init.org::*my-org-brain-entry-path][my-org-brain-entry-path:1]]
(defun my-org-brain-entry-path (entry)
  "Return the parent directory for Org-Brain titles.
ENTRY should be the title string of the entry.

Returns the parent directory as string, formatted for the Org-brain buffer."
  (org-with-point-at (org-brain-entry-marker entry)
    (concat (file-name-base
             (string-trim (file-name-directory (buffer-file-name)) nil "/"))
            ":")))
;; my-org-brain-entry-path:1 ends here

;; [[file:init.org::*org-brain][org-brain:1]]
(use-package org-brain
  :requires (ascii-art-to-unicode)
  :init
  (setq org-brain-path "~/cloud/brain")
  (bind-key "C-c b" 'org-brain-prefix-map org-mode-map)
  (setq org-id-track-globally t)
  ;; (setq org-id-locations-file "~/.emacs.d/.org-id-locations")
  (add-hook 'before-save-hook #'org-brain-ensure-ids-in-buffer)
  (push '("b" "Brain" plain (function org-brain-goto-end)
          "* %i%?" :empty-lines 1)
        org-capture-templates)
  (setq org-brain-visualize-default-choices 'all)
  (setq org-brain-title-max-length 12)
  (setq org-brain-include-file-entries t
        org-brain-file-entries-use-title t)
  ;; (add-hook 'org-brain-visualize-mode-hook #'org-brain-polymode)  ;; use with polymode package
;; org-brain:1 ends here

;; [[file:init.org::*Show the parent directory][Show the parent directory:1]]
(add-hook 'org-brain-vis-title-prepend-functions #'my-org-brain-entry-path)
;; Show the parent directory:1 ends here

;; [[file:init.org::*End use-package][End use-package:1]]
)
;; End use-package:1 ends here

;; [[file:init.org::*org-brain-insert-resource-icon][org-brain-insert-resource-icon:1]]
(unless (am-i-on-cellphone-p)
  (defun org-brain-insert-resource-icon (link)
    "Insert an icon, based on content of org-mode LINK."
    (insert (format "%s "
                  (cond ((string-prefix-p "http" link)
                         (cond ((string-match "wikipedia\\.org" link)
                                (all-the-icons-faicon "wikipedia-w"))
                               ((string-match "github\\.com" link)
                                (all-the-icons-octicon "mark-github"))
                               ((string-match "vimeo\\.com" link)
                                (all-the-icons-faicon "vimeo"))
                               ((string-match "youtube\\.com" link)
                                (all-the-icons-faicon "youtube"))
                               (t
                                (all-the-icons-faicon "globe"))))
                        ((string-prefix-p "brain:" link)
                         (all-the-icons-fileicon "brain"))
                        (t
                         (all-the-icons-icon-for-file link))))))

  (add-hook 'org-brain-after-resource-button-functions #'org-brain-insert-resource-icon))
;; org-brain-insert-resource-icon:1 ends here

;; [[file:init.org::*aa2u-buffer][aa2u-buffer:1]]
(defun aa2u-buffer ()
  (aa2u (point-min) (point-max)))

(add-hook 'org-brain-after-visualize-hook #'aa2u-buffer)
;; aa2u-buffer:1 ends here

;; [[file:init.org::*Accept enumerations with letters][Accept enumerations with letters:1]]
(setq org-list-allow-alphabetical t)
;; Accept enumerations with letters:1 ends here

;; [[file:init.org::*ob-prolog][ob-prolog:1]]
(use-package ob-prolog
  :ensure t)
;; ob-prolog:1 ends here

;; [[file:init.org::*Enable languages][Enable languages:1]]
(defvar my-org-no-confirm-evaluation '("latex" "ditaa" "dot" "emacs-lisp" "elisp" "lilypond")
  "Languages that Org Babel can execute without user confirmation.")

(defun my-org-confirm-babel-evaluate (lang _body)
  "Function to test if LANG should be confirmed for evaluation or not.
Return t if it should be confirmed, nil otherwise.

Intended for `org-confirm-babel-evaluate'." 
  (not (member lang my-org-no-confirm-evaluation)))

(setq org-confirm-babel-evaluate 'my-org-confirm-babel-evaluate)
;; Enable languages:1 ends here

;; [[file:init.org::*Enable languages][Enable languages:2]]
(org-babel-do-load-languages 'org-babel-load-languages
                             '((emacs-lisp . t)
                               (dot . t)
                               (latex . t)))
;; Enable languages:2 ends here

;; [[file:init.org::*Other Configuration][Other Configuration:1]]
(setq org-hide-emphasis-markers nil
      org-preview-latex-default-process 'imagemagick)
;; Other Configuration:1 ends here

;; [[file:init.org::*my-add-org-file-apps][my-add-org-file-apps:1]]
(defun my-add-org-file-apps (file-identifier command)
  "Function to ensure a unique association of file and commands.
The `org-file-apps' variable use cons of FILE-IDENTIFIER and COMMANDS.
But when adding, they can be duplicated.  This ensure to delete any
possible association and to add the corret identifier and command.

According to the variable documentation:
- FILE-IDENTIFIER can be a symbol or string.
- COMMANDS is a symbol."
  (delq (assoc file-identifier org-file-apps) org-file-apps)
  (push (cons file-identifier command) org-file-apps))
;; my-add-org-file-apps:1 ends here

;; [[file:init.org::*Support for Mindmap and HTML files][Support for Mindmap and HTML files:1]]
(my-add-org-file-apps "\\.mm\\'" 'default)
(my-add-org-file-apps "\\.x?html?\\'" 'default)
;; Support for Mindmap and HTML files:1 ends here

;; [[file:init.org::*Open videos with mpv][Open videos with mpv:1]]
(my-add-org-file-apps "\\.mp4\\'" "mpv %s")
(my-add-org-file-apps "\\.ogv\\'" "mpv %s")
(my-add-org-file-apps "\\.ogg\\'" "mpv %s")
;; Open videos with mpv:1 ends here

;; [[file:init.org::*Open in dired, not in Explorer][Open in dired, not in Explorer:1]]
(when (eq system-type 'windows-nt)
  (my-add-org-file-apps 'directory 'emacs)
  (my-add-org-file-apps "file+sys" 'system))
;; Open in dired, not in Explorer:1 ends here

;; [[file:init.org::*Inline images in white background][Inline images in white background:1]]
;; Background color in inlines images
(defcustom org-inline-image-background "white"
  "The color used as the default background for inline images.
  When nil, use the default face background."
  :group 'org
  :type '(choice color (const nil)))

(defun org-display-inline-images--with-color-theme-background-color (args)
  "Specify background color of Org-mode inline image through modify `ARGS'."
  (let* ((file (car args))
         (type (cadr args))
         (data-p (caddr args))
         (props (cdddr args)))
    ;; get this return result style from `create-image'
    (append (list file type data-p)
            (list :background org-inline-image-background)
            props)))

(advice-add 'create-image :filter-args
            #'org-display-inline-images--with-color-theme-background-color)
;; Inline images in white background:1 ends here

;; [[file:init.org::*Org-ref references and citation][Org-ref references and citation:1]]
(use-package org-ref
  :if (not (am-i-on-cellphone-p))
  :commands (org-ref-insert-link
             org-ref-insert-cite-link
             org-ref-insert-label-link
             org-ref-insert-ref-link
             org-ref-citation-hydra/body
             org-ref-help)    
  :init
  (setq org-ref-completion-library 'org-ref-ivy-cite) )
;; Org-ref references and citation:1 ends here

;; [[file:init.org::*Org mind map][Org mind map:1]]
(use-package org-mind-map
  :commands (org-mind-map-write 
             org-mind-map-current-branch
             org-mind-map-current-tree))
;; Org mind map:1 ends here

;; [[file:init.org::*Org Navigation][Org Navigation:1]]
(use-package orgnav
  :defer t
  :commands (orgnav-search-ancestors
             orgnav-search-subtree
             orgnav-search-root
             orgnav-search-nearby))
;; Org Navigation:1 ends here

;; [[file:init.org::*Add standalone class][Add standalone class:1]]
(unless (assoc "standalone" org-latex-classes)
  (push '("standalone" "\\documentclass[11pt]{standalone}"
          ("\\section{%s}" . "\\section*{%s}")
          ("\\subsection{%s}" . "\\subsection*{%s}")
          ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
          ("\\paragraph{%s}" . "\\paragraph*{%s}")
          ("\\subparagraph{%s}" . "\\subparagraph*{%s}"))
        org-latex-classes))
;; Add standalone class:1 ends here

;; [[file:init.org::*Latex Compilation][Latex Compilation:1]]
(setq org-latex-pdf-process
      '("latexmk -g -pdf -pdflatex=\"%latex\" -shell-escape -outdir=%o %f"))
;; Latex Compilation:1 ends here

;; [[file:init.org::*Latex Compilation][Latex Compilation:2]]
(setq org-latex-packages-alist
      '(("AUTO" "polyglossia" nil ("xelatex" "lualatex"))
        ("AUTO" "babel" t ("pdflatex"))))
;; Latex Compilation:2 ends here

;; [[file:init.org::*Show compilation buffer in other window][Show compilation buffer in other window:1]]
(unless (assoc "*tex-shell*" display-buffer-alist)
  (push (cons "*tex-shell*" '(display-buffer-in-direction . ((window . main)
                                                             (direction . right))))
        display-buffer-alist))
;; Show compilation buffer in other window:1 ends here

;; [[file:init.org::*Capture templates][Capture templates:1]]
(setq org-capture-templates '())
;; Capture templates:1 ends here

;; [[file:init.org::*my-add-capture-template][my-add-capture-template:1]]
(defun my-add-capture-template (key name &optional org-headline)
  "Add a predefined capture template."
  (let ((headline (if org-headline
                      org-headline
                    (concat name " Captures"))))
    (push `(,key ,name entry
                 (file+headline "~/Sync/orgs/todoes.org" ,headline)
                 "** %^{title} :capture:%^g:
:CAPTUREINFO:
- Date :: %U
- Buffer :: %f
- Annotation :: %a
:END:
%i
%?")
          org-capture-templates)))
;; my-add-capture-template:1 ends here

;; [[file:init.org::*Research][Research:1]]
(my-add-capture-template "r" "Research")
;; Research:1 ends here

;; [[file:init.org::*Teaching][Teaching:1]]
(my-add-capture-template "t" "Teaching")
;; Teaching:1 ends here

;; [[file:init.org::*MCS Thesis][MCS Thesis:1]]
(my-add-capture-template "m" "MCS")
;; MCS Thesis:1 ends here

;; [[file:init.org::*Emacs][Emacs:1]]
(my-add-capture-template "e" "Emacs")
;; Emacs:1 ends here

;; [[file:init.org::*Home][Home:1]]
(my-add-capture-template "d" "Domestic")
;; Home:1 ends here

;; [[file:init.org::*General][General:1]]
(my-add-capture-template "g" "General")
;; General:1 ends here

;; [[file:init.org::*Extension][Extension:1]]
(my-add-capture-template "E" "Extension")
;; Extension:1 ends here

;; [[file:init.org::*presentation][presentation:1]]
(use-package presentation
  :commands (presentation-mode)
  :ensure nil)
;; presentation:1 ends here

;; [[file:init.org::*moom][moom:1]]
(use-package moom
  :if (not (am-i-on-cellphone-p))
  :defer t)
;; moom:1 ends here

;; [[file:init.org::*centered-window][centered-window:1]]
(use-package centered-window
  :ensure t)
;; centered-window:1 ends here

;; [[file:init.org::*org-tree-slide][org-tree-slide:1]]
(use-package org-tree-slide)
;; org-tree-slide:1 ends here

;; [[file:init.org::*org-tree-slide-pauses][org-tree-slide-pauses:1]]
(use-package org-tree-slide-pauses
  :after org-tree-slide)
;; org-tree-slide-pauses:1 ends here

;; [[file:init.org::*interaction-log][interaction-log:1]]
(use-package interaction-log
  :ensure t)
;; interaction-log:1 ends here

;; [[file:init.org::*Clocks][Clocks:1]]
(setq org-clock-persist 'history)
(org-clock-persistence-insinuate)
;; Clocks:1 ends here

;; [[file:init.org::*my-org-lp-goto-error][my-org-lp-goto-error:1]]
(defun my-org-lp-goto-error (oldfn &optional prefix &rest args)
  "Make `compile-goto-error' lead to an Org literate program, if present.
This is meant to be used as `:around' advice for `compile-goto-error'.
OLDFN is `compile-goto-error'.
With PREFIX arg, just run `compile-goto-error' as though unadvised.
ARGS are ignored."
  (interactive "P")
  (if prefix
      (funcall oldfn)
    (let (buffer position column tangled-file-exists-p)
      (save-window-excursion
        (funcall oldfn)
        (setq column (- (point) (line-beginning-position)))
        ;; `compile-goto-error' might be called from the output of
        ;; `literate-elisp-byte-compile-file', which means
        ;; `org-babel-tangle-jump-to-org' would error
        (when (ignore-errors (org-babel-tangle-jump-to-org))
          (setq buffer         (current-buffer)
                position       (point)
                tangled-file-exists-p t)))
      ;; back to where we started - the `compilation-mode' buffer
      (if tangled-file-exists-p
          (let ((org-window (get-buffer-window buffer)))
            ;; if the Org buffer is visible, switch to its window
            (if (window-live-p org-window)
                (select-window org-window)
              (switch-to-buffer buffer))
            (goto-char (+ position column)))
        (funcall oldfn)))))

(advice-add 'compile-goto-error :around #'my-org-lp-goto-error)
;; (advice-remove 'compile-goto-error #'my-org-lp-goto-error)
;; my-org-lp-goto-error:1 ends here

;; [[file:init.org::*Update syntax][Update syntax:1]]
(org-element-update-syntax)
;; Update syntax:1 ends here

;; [[file:init.org::*graphviz-dot-mode][graphviz-dot-mode:1]]
(use-package graphviz-dot-mode
  :if (not (am-i-on-cellphone-p))
  :mode "\\.gv\\'" "\\.dot\\'")
;; graphviz-dot-mode:1 ends here

;; [[file:init.org::*sparql-mode][sparql-mode:1]]
(use-package sparql-mode
  :if (not (am-i-on-cellphone-p))
  :mode "\\.sparql\\'"
  :config
;; sparql-mode:1 ends here

;; [[file:init.org::*org-babel][org-babel:1]]
(org-babel-do-load-languages
 'org-babel-load-languages
 '((sparql . t)))
;; org-babel:1 ends here

;; [[file:init.org::*End use-package][End use-package:1]]
) ;; use-package
;; End use-package:1 ends here

;; [[file:init.org::*gnuplot][gnuplot:1]]
(use-package gnuplot-mode
  :if (not (am-i-on-cellphone-p))
  :mode "\\.gnuplot\\'" "\\.gp\\'")
;; gnuplot:1 ends here

;; [[file:init.org::*iRFC][iRFC:2]]
(unless (am-i-on-cellphone-p)
  (require 'irfc)
  (setq irfc-directory "~/.emacs.d/irfc")
  (when (not (file-exists-p irfc-directory))
    (make-directory irfc-directory))
  (setq irfc-assoc-mode t))
;; iRFC:2 ends here

;; [[file:init.org::*Yasnippet][Yasnippet:1]]
(use-package yasnippet
  :if (not (am-i-on-cellphone-p))
  :config
  (add-to-list 'yas-snippet-dirs "~/emacs-stuff/snippets")
  (yas-reload-all)
  (yas-global-mode))
;; Yasnippet:1 ends here

;; [[file:init.org::*pdf-tools][pdf-tools:2]]
(use-package pdf-tools
  :if (not (am-i-on-cellphone-p))
  :mode "\\.pdf\\'"
  :config 
  (pdf-loader-install t t))
;; pdf-tools:2 ends here

;; [[file:init.org::*org-noter][org-noter:1]]
(use-package org-noter
  :if (not (am-i-on-cellphone-p))
  :mode "\\.pdf\\'"
  :after (pdf-tools))
;; org-noter:1 ends here

;; [[file:init.org::*Markdown][Markdown:1]]
(use-package markdown-mode
  :mode "\\.md\\'" "\\.markdown\\'"
  :if (not (am-i-on-cellphone-p)))
;; Markdown:1 ends here

;; [[file:init.org::*Emojify][Emojify:1]]
(use-package emojify
  :if (not (am-i-on-cellphone-p))
  :commands (emojify-insert-emoji emojify-apropos-emoji))
;; Emojify:1 ends here

;; [[file:init.org::*emoji cheat sheet plus][emoji cheat sheet plus:1]]
(use-package emoji-cheat-sheet-plus
  :if (not (am-i-on-cellphone-p))
  :commands (emoji-cheat-sheet-plus-insert emoji-cheat-sheet-plus-buffer))
;; emoji cheat sheet plus:1 ends here

;; [[file:init.org::*helm-bibtex][helm-bibtex:1]]
(use-package helm-bibtex
  :if (not (am-i-on-cellphone-p))
  :commands (helm-bibtex)
  :after org-ref)
;; helm-bibtex:1 ends here

;; [[file:init.org::*Bibtex Ebib][Bibtex Ebib:1]]
(use-package ebib
  :if (not (am-i-on-cellphone-p))
  :commands (ebib)
  :defer t
  :config
  (setq ebib-bib-search-dirs '("~/cloud/resources/bibtexs")
        ebib-file-associations '(("pdf")
                                 ("ps" . "gv"))
        ebib-file-search-dirs '("~/cloud/resources/")))
;; Bibtex Ebib:1 ends here

;; [[file:init.org::*Adding new compiling options][Adding new compiling options:1]]
(with-eval-after-load "lilypond-mode"
  (add-to-list 'lilypond-command-alist
               '("PNG Cropped" "lilypond -dcrop %s")))
;; Adding new compiling options:1 ends here

;; [[file:init.org::*Set SWI][Set SWI:1]]
(use-package prolog-mode
  :ensure nil
  :mode "\\.pl\\'"
  :config
  (setq auto-mode-alist (append '(("\\.pl$" . prolog-mode))
                                auto-mode-alist)
        prolog-system 'swi))
;; Set SWI:1 ends here

;; [[file:init.org::*ediprolog][ediprolog:1]]
(use-package ediprolog
  :mode "\\.pl\\'"
  :after (prolog-mode))
;; ediprolog:1 ends here

;; [[file:init.org::*my-ciao-load-site][my-ciao-load-site:1]]
(defun my-ciao-load-site ()
  "Load the Ciao Prolog environment"
  (interactive)
  (if (file-exists-p
       "~/.ciaoroot/master/ciao_emacs/elisp/ciao-site-file.el")
      (load-file
       "~/.ciaoroot/master/ciao_emacs/elisp/ciao-site-file.el"))
  (when (not (member "~/.ciaoroot/master/build/doc" Info-directory-list))
    (add-to-list 'Info-directory-list "~/.ciaoroot/master/build/doc")))
;; my-ciao-load-site:1 ends here

;; [[file:init.org::*python - Begin use-package][python - Begin use-package:1]]
(use-package python
  :ensure t
  :defer t
  :commands (python-mode)
  :mode ("\\.py\\'" . python-mode)
  :config
;; python - Begin use-package:1 ends here

;; [[file:init.org::*Config][Config:1]]
(setq python-check-command "flake8"
      python-shell-interpreter "python3"
      elpy-rpc-python-command "python3")

(elpy-enable)
;; Config:1 ends here

;; [[file:init.org::*Config][Config:2]]
;; (elpy-mode)
(unless (member 'elpy-mode python-mode-hook)
  (add-hook 'python-mode-hook 'elpy-mode))
;; Config:2 ends here

;; [[file:init.org::*End use-package][End use-package:1]]
) ;; use-package
;; End use-package:1 ends here

;; [[file:init.org::*Pydoc][Pydoc:1]]
(use-package pydoc
  :if (not (am-i-on-cellphone-p))
  :commands (pydoc pydoc-browse)
  :after elpy)
;; Pydoc:1 ends here

;; [[file:init.org::*Ada Ref Man][Ada Ref Man:1]]
(use-package ada-ref-man
  :if (not (am-i-on-cellphone-p))
  :defer t
  :after (ada-mode))
;; Ada Ref Man:1 ends here

;; [[file:init.org::*package-lint-flymake][package-lint-flymake:1]]
(use-package package-lint-flymake
  :defer t
  :if (not (am-i-on-cellphone-p)))
;; package-lint-flymake:1 ends here

;; [[file:init.org::*yari][yari:1]]
(use-package yari
  :commands (yari yari-helm)
  :config
  ;; (setq yari-ri-program-name "ri --doc-dir=/usr/share/ri/system")
  :if (not (am-i-on-cellphone-p)))
;; yari:1 ends here

;; [[file:init.org::*inf-ruby][inf-ruby:1]]
(use-package inf-ruby
  :if (not (am-i-on-cellphone-p))
  :commands (inf-ruby)
  :config
  (setq inf-ruby-prompt-format "irb([^)]+):[0-9:]*> "))
;; inf-ruby:1 ends here

;; [[file:init.org::*ruby-block][ruby-block:2]]
(unless (am-i-on-cellphone-p)
  (require 'ruby-block))
;; ruby-block:2 ends here

;; [[file:init.org::*ruby-tools][ruby-tools:1]]
(use-package ruby-tools
  :if (not (am-i-on-cellphone-p))
  :commands (ruby-tools-mode))
;; ruby-tools:1 ends here

;; [[file:init.org::*rvm][rvm:1]]
(use-package rvm
  :if (not (am-i-on-cellphone-p))
  :commands (rvm-use
             rvm-use-default
             rvm-open-gem
             rvm-activate-corresponding-ruby))
;; rvm:1 ends here

;; [[file:init.org::*yard][yard:1]]
(use-package yard-mode
  :if (not (am-i-on-cellphone-p))
  :defer t
  :config
  (add-hook 'ruby-mode-hook 'yard-mode)
  (add-hook 'ruby-mode-hook 'eldoc-mode))
;; yard:1 ends here

;; [[file:init.org::*crystal-mode][crystal-mode:1]]
(use-package crystal-mode
  :if (not (am-i-on-cellphone-p))
  :defer t)
;; crystal-mode:1 ends here

;; [[file:init.org::*ameba][ameba:1]]
(use-package ameba
  :if (not (am-i-on-cellphone-p))
  :defer t)
;; ameba:1 ends here

;; [[file:init.org::*flycheck-crystal][flycheck-crystal:1]]
(use-package flycheck-crystal
  :if (not (am-i-on-cellphone-p))
  :defer t)
;; flycheck-crystal:1 ends here

;; [[file:init.org::*flycheck-ameba][flycheck-ameba:1]]
(use-package flycheck-ameba
  :if (not (am-i-on-cellphone-p))
  :defer t)
;; flycheck-ameba:1 ends here

;; [[file:init.org::*Haml][Haml:1]]
(use-package haml-mode
  :mode "\\.haml\\'"
  :config
  (add-hook 'haml-mode-hook
            (lambda ()
              (setq indent-tabs-mode nil)
              (define-key haml-mode-map "\C-m" 'newline-and-indent))))
;; Haml:1 ends here

;; [[file:init.org::*Begin use-package][Begin use-package:1]]
(use-package web-mode
  :if (not (am-i-on-cellphone-p))
  :mode (;;  "\\.php\\'"
         "\\.hbs\\'"
         "\\.[sx]?html?\\(\\.[a-zA-Z_]+\\)?\\'")
  :config
;; Begin use-package:1 ends here

;; [[file:init.org::*Load the web-mode][Load the web-mode:1]]
;;  (push '("\\.php$" . web-mode) auto-mode-alist)
(push '("\\.hbs$" . web-mode) auto-mode-alist)

(let ((elt (assoc "\\.[sx]?html?\\(\\.[a-zA-Z_]+\\)?\\'" auto-mode-alist)))
  (setq auto-mode-alist (remove elt auto-mode-alist)))

(push '("\\.[sx]?html?\\(\\.[a-zA-Z_]+\\)?\\'" . web-mode) auto-mode-alist)
;; Load the web-mode:1 ends here

;; [[file:init.org::*Configure and personalizations][Configure and personalizations:1]]
(setq web-mode-markup-indent-offset 2
      web-mode-attr-value-indent-offset 2
      web-mode-code-indent-offset 4)
;; Configure and personalizations:1 ends here

;; [[file:init.org::*End use-package][End use-package:1]]
) ;; use-package
;; End use-package:1 ends here

;; [[file:init.org::*Web-narrow-mode][Web-narrow-mode:1]]
(use-package web-narrow-mode
  :if (not (am-i-on-cellphone-p))
  :commands (web-narrow-to-block web-narrow-to-region web-narrow-to-element)
  ;; :bind
  ;; ("C-c C-u j" . web-narrow-to-block)
  ;; ("C-c C-u l" . web-narrow-to-region)
  ;; ("C-c C-u u" . web-narrow-to-element)
  :after (:all web-mode)
  :config
  (add-hook 'web-mode-hook 'web-narrow-mode))
;; Web-narrow-mode:1 ends here

;; [[file:init.org::*php-mode][php-mode:1]]
(use-package php-mode
  :if (not (am-i-on-cellphone-p))
  :defer t
  :commands (php-mode))
;; php-mode:1 ends here

;; [[file:init.org::*php-eldoc][php-eldoc:1]]
(use-package php-eldoc
  :if (not (am-i-on-cellphone-p))
  :commands (php-eldoc-enable))
;; php-eldoc:1 ends here

;; [[file:init.org::*scss-mode][scss-mode:1]]
(use-package scss-mode
  :if (not (am-i-on-cellphone-p))
  :mode "\\.scss\\'") ;; use-package
;; scss-mode:1 ends here

;; [[file:init.org::*c-mode][c-mode:1]]
(use-package c-mode
  :if (not (am-i-on-cellphone-p))
  :ensure nil
  :mode "\\.cu\\'"
  :config
  (setq auto-mode-alist (append '(("\\.cu$" . c-mode))
                                auto-mode-alist)))
;; c-mode:1 ends here

;; [[file:init.org::*vala-mode][vala-mode:1]]
(use-package vala-mode
  :if (not (am-i-on-cellphone-p))
  :mode "\\.vala\\'" "\\.vapi\\'")
;; vala-mode:1 ends here

;; [[file:init.org::*ttl-mode][ttl-mode:1]]
(use-package ttl-mode
  :mode "\\.ttl\\'")
;; ttl-mode:1 ends here

;; [[file:init.org::*yaml-mode][yaml-mode:1]]
(use-package yaml-mode
  :mode "\\.yaml\\'")
;; yaml-mode:1 ends here

;; [[file:init.org::*haskell-mode][haskell-mode:1]]
(use-package haskell-mode
  :if (not (am-i-on-cellphone-p))
  :mode "\\.hl\\'"
  :init
  (add-hook 'haskell-mode-hook 'turn-on-haskell-indentation)
  (setq haskell-program-name "ghc --interactive"))
;; haskell-mode:1 ends here

;; [[file:init.org::*fish-mode][fish-mode:1]]
(use-package fish-mode
  :ensure t
  :mode "\\.fish\\'")
;; fish-mode:1 ends here

;; [[file:init.org::*coffee-mode][coffee-mode:1]]
(use-package coffee-mode
  ;; :defer t
  :if (not (am-i-on-cellphone-p))
  :mode "\\.coffee\\'"
  :init 
  (setq coffee-tab-width 4))
;; coffee-mode:1 ends here

;; [[file:init.org::*nginx-mode][nginx-mode:1]]
(use-package nignx-mode
  :if (not (am-i-on-cellphone-p))
  :ensure nil
  :mode "nginx\\.conf\\'" "\\.nginx")
;; nginx-mode:1 ends here

;; [[file:init.org::*apache-mode][apache-mode:1]]
(use-package apache-mode
  :if (not (am-i-on-cellphone-p))
  :ensure nil
  :mode 
  "/apache2/sites-\\(?:available\\|enabled\\)/"
  "/httpd/conf/.+\\.conf\\'"
  "/apache2/.+\\.conf\\'"
  "/\\(?:access\\|httpd\\|srm\\)\\.conf\\'"
  "/\\.htaccess\\'")
;; apache-mode:1 ends here

;; [[file:init.org::*lua-mode][lua-mode:1]]
(use-package lua-mode
  :if (not (am-i-on-cellphone-p))
  :mode "\\.lua\\'")
;; lua-mode:1 ends here

;; [[file:init.org::*Conkeror-minor-mode][Conkeror-minor-mode:1]]
(use-package conkeror-minor-mode
  :if (not (am-i-on-cellphone-p))
  :ensure nil
  :commands (conkeror-minor-mode))
;; Conkeror-minor-mode:1 ends here

;; [[file:init.org::*ess][ess:1]]
(use-package ess
  :if (not (am-i-on-cellphone-p))
  :commands (ess-mode))
;; ess:1 ends here

;; [[file:init.org::*racket-mode][racket-mode:1]]
(use-package racket-mode
  :mode ".rkt"
  :hook
  ((racket-mode . geiser-mode)
   ;; (racket-mode . paredit-mode)
   (racket-mode . auto-complete-mode)))
;; racket-mode:1 ends here

;; [[file:init.org::*Completions is to narrow!][Completions is to narrow!:1]]
(defun my-large-splited-windows ()
  "Make splited windows larger!
Modify the current min height to make splited windows larger.  This
mean that using `display-buffer-at-bottom' or
`display-buffer-below-selecte' will make buffers with larger windows.

This only affects the local buffer only."
  (setq-local window-min-height 10))

(add-hook 'sly-mrepl-mode-hook #'my-large-splited-windows)
;; Completions is to narrow!:1 ends here

;; [[file:init.org::*Projectile][Projectile:1]]
(use-package projectile
  :defer t
  :bind-keymap
  ("s-p" . projectile-command-map)
  ("C-c p" . projectile-command-map)
  :config
  ;; (define-key projectile-mode-map (kbd "s-p") 'projectile-command-map)
  ;; (define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)
  (projectile-mode +1))
;; Projectile:1 ends here

;; [[file:init.org::*my-calendar-nth-day][my-calendar-nth-day:1]]
(defun my-calendar-nth-day (nth-day)
  "Return the nth day from the current day.
Calculate the NTH-DAY and return it in a calendar
format: (month day year)."
  (let ((year (nth 5 (decode-time))))
    (calendar-gregorian-from-absolute
     (+ -1 nth-day (calendar-absolute-from-gregorian (list 1 1 year))))))
;; my-calendar-nth-day:1 ends here

;; [[file:init.org::*Holidays][Holidays:1]]
(setq holiday-general-holidays nil)
(setq holiday-other-holidays
      '(;; March
        (holiday-fixed 3 3 "Carnaval")
        (holiday-fixed 3 4 "Carnaval")
        (holiday-fixed 3 24 "Día de la memoria por la verdad y justicia")
        ;; April
        (holiday-fixed 4 2 "Día del veterano y de los caídos en la Guerra de Malvinas")
        (holiday-fixed 4 24
                       "Día por la tolerancia y el respeto entre los pueblos")
        ;; May
        (holiday-fixed 5 1 "Día del trabajador")
        (holiday-fixed 5 25 "Revolución de Mayo")
        ;; June
        (holiday-fixed 6 20 "Manuel Belgrano")
        ;; July
        (holiday-fixed 7 9 "Día de la independencia")
        ;; August
        (if (equal (calendar-day-of-week (list 8 17 (nth 5 (decode-time)))) 0)
             ;; Cae un domingo, mover a lunes.
             (holiday-fixed 8 18 "José de San Martín")
           (holiday-fixed 8 17 "José de San Martín"))
        ;; September
        (holiday-fixed 9 17 "Día del profesor")
        (holiday-fixed 9 21 "Día de la primavera y del estudiante")
        ;; October
        (holiday-float 10 2 2 "Ada Lovelace Day")
        (if (equal (calendar-day-of-week (list 10 12 (nth 5 (decode-time)))) 0)
             (holiday-fixed 10 13 "Día del respeto a la diversidad cultural")
           (holiday-fixed 10 12 "Día del respeto a la diversidad cultural"))
        ;; November
        (if (equal (calendar-day-of-week (list 11 20 (nth 5 (decode-time)))) 0)
            (holiday-fixed 11 21 "Día de la Soberanía Nacional")
          (holiday-fixed 11 20 "Día de la Soberanía Nacional"))

        (holiday-fixed 12 9 "Día internacional de la informática")
        (holiday-fixed 12 11 "Día nacional del profesional informático")
        ;; Último viernes de julio
        (holiday-float 7 5 -1 "Día del SysAdmin")

        ;; 256° día del año
        (holiday-sexp '(my-calendar-nth-day 256) "Día del programador")))

(setq holiday-local-holidays nil)
(setq holiday-hebrew-holidays nil)
(setq holiday-bahai-holidays nil)
(setq holiday-islamic-holidays nil)
(setq holiday-oriental-holidays nil)

;; Show marks
(setq calendar-mark-diary-entries-flag t)
(setq calendar-mark-holidays-flag t)
;; Holidays:1 ends here

;; [[file:init.org::*Dired async call fix][Dired async call fix:1]]
(defun my-dired-shell-stuff-it-add (x)
  "Add a space between the command and the wait.
If the X command ends with \"&wait&\", then add a space before the ampersand."
  (replace-regexp-in-string "&wait&$" " &wait&" x))

(advice-add #'dired-shell-stuff-it :filter-return #'my-dired-shell-stuff-it-add)
;; Dired async call fix:1 ends here

;; [[file:init.org::*Emms][Emms:1]]
(use-package emms
  :if (not (am-i-on-cellphone-p))
  :commands (emms)
  :config
  (require 'emms-setup)
  (emms-all)
  (emms-default-players)

  (setq
   emms-mode-line-mode-line-function (quote emms-mode-line-icon-function)
   emms-player-list (quote
                     (emms-player-mpv
                      emms-player-mplayer-playlist
                      emms-player-mplayer
                      emms-player-vlc
                      emms-player-vlc-playlist))
   emms-player-mpg321-command-name "mplayer"))
;; Emms:1 ends here

;; [[file:init.org::*Move init file][Move init file:1]]
(setq gnus-init-file (emacsstuff-dir "gnus/gnus-init.el"))
;; Move init file:1 ends here

;; [[file:init.org::*Tree appearance on summary buffer][Tree appearance on summary buffer:1]]
(setq gnus-thread-indent-level 4 ;; Not used really, you nee %I in line format

    ;; Make a false root per subject
    gnus-summary-make-false-root-always t
    gnus-summary-make-false-root 'dummy
    
    ;; Default... no idea what is the difference
    gnus-generate-tree-function #'gnus-generate-vertical-tree

    ;; Tree characters
    ;; gnus-sum-thread-tree-single-leaf "\\->"
    ;; gnus-sum-thread-tree-leaf-with-other "+->"
    ;; gnus-sum-thread-tree-vertical "|"
    ;; gnus-sum-thread-tree-indent "  "
    gnus-sum-thread-tree-single-leaf "└─→ "
    gnus-sum-thread-tree-leaf-with-other "├─→ "
    gnus-sum-thread-tree-vertical "|"
    gnus-sum-thread-tree-indent "  "
    gnus-thread-tree-root "> "
    gnus-sum-thread-tree-false-root "> "

    ;; Line formats...
    gnus-summary-dummy-line-format "_>> %S <<\n"
    gnus-summary-line-format "%B%U%R%z %(%[%4L: %n%]%) %s\n")
;; Tree appearance on summary buffer:1 ends here

;; [[file:init.org::*Jabber][Jabber:1]]
(setq jabber-msg-sound (emacsstuff-dir "personal/receive.wav"))
(setq jabber-default-priority 10)
(setq jabber-default-show "dnd")
(setq jabber-default-status (my-secrets-get "jabber-default-status"))
(add-hook 'jabber-roster-mode-hook 'jabber-autoaway-start)
;; Jabber:1 ends here

;; [[file:init.org::*Ebib][Ebib:1]]
(use-package ebib
  :commands (ebib)
  :defer t
  :config
  (setq ebib-bib-search-dirs '("~/cloud/resources/bibtexs")
        ebib-file-associations '(("pdf")
                                 ("ps" . "gv"))
        ebib-file-search-dirs '("~/cloud/resources/")))
;; Ebib:1 ends here

;; [[file:init.org::*my-wl-copy-href-link][my-wl-copy-href-link:1]]
(defun my-wl-copy-href-link ()
  "Copy the URL from a message buffer."
  (interactive)
  (kill-new (get-text-property (point) 'w3m-href-anchor)))
;; my-wl-copy-href-link:1 ends here

;; [[file:init.org::*my-wl-browse-href-url][my-wl-browse-href-url:1]]
(defun my-wl-browse-href-url (&optional copy)
  "Open browser the link from the current point in a MIME-view buffer.
Get the link from the current point and open the browser.
With C-u store it in the clipboard."  
  (interactive "P")
  (let ((url (get-text-property (point) 'w3m-href-anchor)))
    (if copy
        (kill-new url)
      (browse-url url))))
;; my-wl-browse-href-url:1 ends here

;; [[file:init.org::*wanderlust - use-package][wanderlust - use-package:1]]
(use-package wanderlust
  :if (not (am-i-on-cellphone-p))
  :commands (wl)
  :config
  (setq wl-folders-file (emacsstuff-dir "secrets/wanderlust/folders"))
;; wanderlust - use-package:1 ends here

;; [[file:init.org::*Close use-package][Close use-package:1]]
) ;; use-package
;; Close use-package:1 ends here

;; [[file:init.org::*Warn the user about uncompface][Warn the user about uncompface:1]]
(unless (executable-find "uncompface")
  (message "Warning: uncompface program was not found and is required by Wanderlust!"))
;; Warn the user about uncompface:1 ends here

;; [[file:init.org::*my/minimal-auto-fill][my/minimal-auto-fill:1]]
(defun my/minimal-auto-fill ()
  "Set the auto-fill to a minimal number.
This would be used on Wanderlust draft buffers."
  (setq fill-column 63))

(add-hook 'wl-draft-mode-hook #'my/minimal-auto-fill)
(add-hook 'wl-draft-mode-hook #'auto-fill-mode)
;; my/minimal-auto-fill:1 ends here

;; [[file:init.org::*Templates][Templates:1]]
(setq wl-template-alist '())
(when (file-exists-p (emacsstuff-dir "secrets/wanderlust/templates.el"))
  (load-library (emacsstuff-dir "secrets/wanderlust/templates.el")))
;; Templates:1 ends here

;; [[file:init.org::*X-Face][X-Face:1]]
(autoload 'x-face-decode-message-header "x-face-e21")
(setq wl-highlight-x-face-function 'x-face-decode-message-header)
;; X-Face:1 ends here

;; [[file:init.org::*Common config][Common config:1]]
(setq wl-from (my-secrets-get "wl-from")
      mime-edit-split-message nil)
;; Common config:1 ends here

;; [[file:init.org::*Use aspell][Use aspell:1]]
(setq ispell-program-name "/usr/bin/aspell")
;; Use aspell:1 ends here

;; [[file:init.org::*Browse-url][Browse-url:1]]
(setq browse-url-browser-function 'browse-url-default-browser)
;; Browse-url:1 ends here

;; [[file:init.org::*Firefox][Firefox:1]]
(setq browse-url-browser-function 'browse-url-firefox)
;; Firefox:1 ends here

;; [[file:init.org::*sqli-add-hooks][sqli-add-hooks:1]]
(defun sqli-add-hooks ()
  "My hooks for sql-interactive-mode"
  (sql-set-product-feature
   'mysql
   :prompt-regexp "^\\(?:mysql\\|MariaDB\\).*> "))

(add-hook 'sql-interactive-mode-hook 'sqli-add-hooks)
;; sqli-add-hooks:1 ends here

;; [[file:init.org::*w3m][w3m:1]]
(use-package w3m
  :commands (w3m))
;; w3m:1 ends here

;; [[file:init.org::*w3][w3:1]]
(use-package w3
  :ensure nil
  :commands (w3))
;; w3:1 ends here

;; [[file:init.org::*Jabber-otr][Jabber-otr:1]]
(use-package jabber-otr
  :ensure nil
  :defer t
  :commands (jabber-otr-encrypt))
;; Jabber-otr:1 ends here

;; [[file:init.org::*neotree][neotree:1]]
(use-package neotree
  :ensure nil
  :commands (neotree
             neotree-show
             neotree-dir
             neotree-find
             neotree-projectile-action
             neotree-toggle))
;; neotree:1 ends here

;; [[file:init.org::*Magit][Magit:1]]
(use-package magit
  :commands (magit))
;; Magit:1 ends here

;; [[file:init.org::*Add Ada info][Add Ada info:1]]
(add-to-list 'Info-additional-directory-list "~/Ada/share/doc/info/")
;; Add Ada info:1 ends here

;; [[file:init.org::*Adding a .local info directory][Adding a .local info directory:1]]
(add-to-list 'Info-additional-directory-list "~/.local/info")
(add-to-list 'Info-additional-directory-list "~/.local/share/info")
;; Adding a .local info directory:1 ends here

;; [[file:init.org::*Add the latex2e info][Add the latex2e info:1]]
(add-to-list 'Info-additional-directory-list
             "/usr/share/texmf-dist/doc/info")
;; Add the latex2e info:1 ends here

;; [[file:init.org::*Add default data][Add default data:1]]
(setq rcirc-default-nick (my-secrets-get "rcirc-default-nick")
      rcirc-default-user-name (my-secrets-get "rcirc-default-user-name")
      rcirc-default-full-name (my-secrets-get "rcirc-default-full-name"))
;; Add default data:1 ends here

;; [[file:init.org::*Add the rcirc server][Add the rcirc server:1]]
(setq rcirc-server-alist '())
;; Add the rcirc server:1 ends here

;; [[file:init.org::*Libera.chat][Libera.chat:1]]
(push '("irc.libera.chat"
        :port 6697
        :encryption tls
        :channels (my-secrets-get "rcirc-liberachat-channels"))
      rcirc-server-alist)
;; Libera.chat:1 ends here

;; [[file:init.org::*Other personal servers][Other personal servers:1]]
(when (file-exists-p (emacsstuff-dir "secrets/rcirc/rcirc.el"))
  (load-library "secrets/rcirc/rcirc.el"))
;; Other personal servers:1 ends here

;; [[file:init.org::*Mastodon client][Mastodon client:1]]
(use-package mastodon
  :if (not (am-i-on-cellphone-p))
  :commands (mastodon mastodon-toot)
  :config
  (setq mastodon-active-user (my-secrets-get "mastodon-active-user"))
  (setq mastodon-instance-url (my-secrets-get "mastodon-instance-url")))
;; Mastodon client:1 ends here

;; [[file:init.org::*default shell is fish][default shell is fish:1]]
(setq shell-file-name (executable-find "fish"))
;; default shell is fish:1 ends here

;; [[file:init.org::*eat][eat:1]]
(use-package eat
  :commands (eat))
;; eat:1 ends here

;; [[file:init.org::*Activate infinite history][Activate infinite history:1]]
(when (file-exists-p "~/repos/fae/fae-inf.el")
  (require 'fae-inf)
  (fae-inf-enable))
;; Activate infinite history:1 ends here

;; [[file:init.org::*Load our own commands][Load our own commands:1]]
(when (file-exists-p (emacsstuff-dir "secrets/eshell/my-commands.el"))
  (load-library (emacsstuff-dir "secrets/eshell/my-commands.el")))
;; Load our own commands:1 ends here

;; [[file:init.org::*world-time-mode][world-time-mode:1]]
(use-package world-time-mode
  :ensure nil
  :commands (world-time-list list-world-time))
;; world-time-mode:1 ends here

;; [[file:init.org::*elpher][elpher:1]]
(use-package elpher
  :commands (elpher)
  :config (setq elpher-default-url-type "gemini"))
;; elpher:1 ends here

;; [[file:init.org::*Lagrange browse-url][Lagrange browse-url:1]]
(defcustom lagrange-program "lagrange"
  "The program path to Lagrange."
  :type 'string)

(defcustom lagrange-arguments nil
  "Extra arguments for Lagrange."
  :type '(list string))

(defconst my-choose-gemini-browser-alist
  (list (cons "elpher" #'elpher-browse-url-elpher)
        (cons "lagrange" #'lagrange-browse-url-lagrange)))

(defun my-choose-gemini-browser (url &rest _)
  "Choose between the Gemini browsers."
  (interactive (browse-url-interactive-arg "URL:"))
  (funcall (alist-get 
            (completing-read "Which browser"
                             (mapcar #'car my-choose-gemini-browser-alist))
            my-choose-gemini-browser-alist
            nil nil #'equal)
           url))

(defun lagrange-browse-url-lagrange (url &rest _)
  "Open Lagrange to browse the given URL."
  (interactive (browse-url-interactive-arg "URL: "))
  (setq url (browse-url-encode-url url))
  (let* ((process-environment (browse-url-process-environment)))
    (apply #'start-process
           (concat "lagrange " url) nil
           lagrange-program
           (append
            lagrange-arguments            
            (list url)))))

(with-eval-after-load 'browse-url
  (add-to-list 'browse-url-handlers 
               (cons "^\\(gopher\\|finger\\|gemini\\)://" 
                     #'my-choose-gemini-browser)))
;; Lagrange browse-url:1 ends here

;; [[file:init.org::*my-tor-switch-link-follow][my-tor-switch-link-follow:1]]
(defun my-tor-switch-link-follow (url &rest _)
  "Copy the link.
Tor cannot be executed with an URL."
  (interactive (browse-url-interactive-arg "URL: "))
  (message url)
  (let ((urlcopy (replace-regexp-in-string "^tor\\(https?\\)" "\\1" url)))
    (kill-new urlcopy)
    (message "Copied to kill-ring: %s" urlcopy)))

(org-link-set-parameters "torhttps" :follow
                         (lambda (url arg)
                           (my-tor-switch-link-follow (concat "https:" url))))
(org-link-set-parameters "torhttp" :follow
                         (lambda (url arg)
                           (my-tor-switch-link-follow (concat "http:" url))))
;; my-tor-switch-link-follow:1 ends here

;; [[file:init.org::*my-reset-buffers][my-reset-buffers:1]]
(defun my-kill-if-non-common (buffer)
  "Kill the buffer provided if not modified and if it is not the initial ones" 
  (unless (member (buffer-name buffer)
                  '("*scratch*" "*Messages*"))
    (kill-buffer-if-not-modified buffer)))

(defun my-reset-buffers ()
  "Kill all saved buffers except the initial ones."
  (interactive)
  (mapcar 'my-kill-if-non-common (buffer-list)))
;; my-reset-buffers:1 ends here

;; [[file:init.org::*my-org-agenda][my-org-agenda:1]]
(defun my-search-clock-reports () 
  "Search #+begin: strings inside the current buffer.

Return a list of positions where these strings can be found."
  (save-excursion
    (goto-char (point-min))
    (let ((lst-results nil))
      (while (re-search-forward "\\(#\\+begin:\\|#\\+BEGIN:\\)" nil t)
        (setq lst-results (push (match-beginning 0) lst-results))
        (goto-char (match-beginning 0))
      (forward-line))
      lst-results)))
;; my-org-agenda:1 ends here

;; [[file:init.org::*my-org-agenda][my-org-agenda:2]]
(defun my-org-agenda ()
  "Show my own style of agenda.
Display the agenda view in list day, and the current work."
  (interactive)
  ;; (with-current-buffer (find-file "~/Sync/orgs/init.org")
  ;;   (dolist (pos (my-search-clock-reports))
  ;;     (goto-char pos)
  ;;     (org-clock-report)))
  (org-agenda-list nil nil 1))
;; my-org-agenda:2 ends here

;; [[file:init.org::*my-init-file][my-init-file:1]]
(defcustom my-init-file "~/emacs-stuff/init.org"
  "Where is my init.org file?"
  :type 'file)
;; my-init-file:1 ends here

;; [[file:init.org::*my-find-init-file][my-find-init-file:1]]
(defun my-find-init-file ()
  "Open the init.org file."
  (interactive)
  (find-file-other-frame my-init-file))
;; my-find-init-file:1 ends here

;; [[file:init.org::*Timezone functions][Timezone functions:1]]
(defun my-convert-timezone (hour from-timezone to-timezone)
  "Convert HOUR hours from the FROM-TIMEZONE to TO-TIMEZONE."
  (interactive "nHour: 
MFrom timezone: 
MTo timezone: ")
  (let ((dtime (decode-time)))
    (setf (decoded-time-zone dtime) from-timezone)
    (setf (decoded-time-hour dtime) hour)
    (message (format "%s <-> %s"
                     (format-time-string "%H:%M %d day %Z %z" (encode-time dtime) from-timezone)
                     (format-time-string "%H:%M %d day %Z %z" (encode-time dtime) to-timezone)))))

(defun my-from-timezone (hour timezone)
  "What time is here whene at TIMEZONE is HOUR hours?
Convert the given HOUR to the provided TIMEZONE.
TIMEZONE should be a string such as \"UTC+10\", \"GMT\",
or a tzdata string like \"America/New_York\"."
    (interactive "nHour:\nMTime-zone string:")
    (my-convert-timezone hour timezone (current-time-zone)))

(defun my-to-timezone (hour timezone)
  "What time is at TIMEZONE when here is HOUR hours?
Convert the given HOUR to the provided TIMEZONE.
TIMEZONE should be a string such as \"UTC+10\", \"GMT\",
or a tzdata string like \"America/New_York\".
HOUR is a number hour."
  (interactive "nHour:\nMTo time-zone string:")
  (my-convert-timezone hour (current-time-zone) timezone))
;; Timezone functions:1 ends here

;; [[file:init.org::*Hydras][Hydras:1]]
(load-library (emacsstuff-dir "my-hydras.el"))
;; Hydras:1 ends here

;; [[file:init.org::*Other configurations][Other configurations:1]]
(when (file-exists-p (emacsstuff-dir "secrets/personal-configurations.el"))
  (load-library (emacsstuff-dir "secrets/personal-configurations.el")))
;; Other configurations:1 ends here

;; [[file:init.org::*Emacs Server][Emacs Server:1]]
(require 'server)
(unless (server-running-p)
  ;; (message"==> Emacs server")
  (setq server-window nil)
  (server-start))
;; Emacs Server:1 ends here

;; [[file:init.org::*Stop the measure of the init time][Stop the measure of the init time:1]]
(setq my-init-stop-time (time-to-seconds))
(message (format "Init.org lasted %f seconds. See `use-package-report'."
                 (- my-init-stop-time my-init-start-time)))
;; Stop the measure of the init time:1 ends here

;; [[file:init.org::*Compile .el files][Compile .el files:1]]
(when (or (not (file-exists-p "~/emacs-stuff/init.elc"))
          (file-newer-than-file-p "~/emacs-stuff/init.el" "~/emacs-stuff/init.elc"))
  
  (when (native-comp-available-p)
    (native-compile-async "~/emacs-stuff/" t nil nil))
  (byte-recompile-directory "~/emacs-stuff/" 0 nil t)
  (message "emacs-stuff: all sources recompiled"))
;; Compile .el files:1 ends here

;; [[file:init.org::*my-cellphone-capture-buttons][my-cellphone-capture-buttons:1]]
(defun my-cellphone-capture-buttons ()
  "Generate a list of button information from `org-capture-templates'.
This button information is for `cptui-create-menu-buffer' or
`cptui-insert-grid-buttons'."
  (mapcar (lambda (c)
            (list (nth 1 c)
                  `(lambda (x) (org-capture nil ,(car c)))))
          org-capture-templates))
;; my-cellphone-capture-buttons:1 ends here

;; [[file:init.org::*my-cellphone-startup-buttons][my-cellphone-startup-buttons:1]]
(defvar my-cellphone-startup-buttons
  (append
   (my-cellphone-capture-buttons)
   '("\n"
     ("Agenda" (lambda (x) (org-agenda-list nil nil 1)))))
  "Buttons for the cellphone menu.")
;; my-cellphone-startup-buttons:1 ends here

;; [[file:init.org::*my-run-cellphone-startup][my-run-cellphone-startup:1]]
(require 'cptui)

(defun my-run-cellphone-startup ()
  "This is the startup function for the cellphone."
  (cptui-create-menu-buffer "Startup" my-cellphone-startup-buttons)
  (my-org-agenda))
;; my-run-cellphone-startup:1 ends here

;; [[file:init.org::last-jobs][last-jobs]]
(cond ((am-i-on-win-p)
       (find-file-read-only "~/startup.org"))
      ((am-i-on-cellphone-p)
       (my-run-cellphone-startup))
      (t 
       ;; (org-brain-visualize "main")
       (my-org-agenda)))
;; last-jobs ends here
