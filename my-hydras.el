;; [[file:my-hydras.org::*Hydra package][Hydra package:1]]
(use-package hydra    
  :ensure t)
;; Hydra package:1 ends here

;; [[file:my-hydras.org::*my/hydra-toggle-verbosity - Function to toggle verbosity][my/hydra-toggle-verbosity - Function to toggle verbosity:1]]
(defun my/hydra-toggle-verbosity (hydra-name)
  "Toggle verbosity of HIDRA-NAME hydra"
  (hydra-set-property hydra-name :verbosity 
                      (if (or (null (hydra-get-property hydra-name :verbosity))
                              (eq 0 (hydra-get-property hydra-name :verbosity)))
                          1
                        0)))
;; my/hydra-toggle-verbosity - Function to toggle verbosity:1 ends here

;; [[file:my-hydras.org::*Main Hydra][Main Hydra:1]]
(defhydra hydra-main (:pre (setq which-key-inhibit t)
                           :post (setq which-key-inhibit nil)
                           :color blue :hint nil)
  "
_s_ → Startup Emacs
_o_ → Org
_g_ → gnus
_j_ → Japanese

_q_ → Quit
"
  ("j" hydra-japanese/body "Japanese")
  ("o" hydra-org-mode/body "Org-mode")
  ("g" hydra-gnus-group/body "gnus group")
  ("s" my-org-agenda "Startup emacs")
  ("q" nil :exit t))

(global-set-key (kbd "<f5>") 'hydra-main/body)
;; Main Hydra:1 ends here

;; [[file:my-hydras.org::*Macro hydra][Macro hydra:1]]
(defhydra hydra-macro (:hint nil :color pink :pre 
                             (when defining-kbd-macro
                                 (kmacro-end-macro 1)))
  "
  ^Create-Cycle^   ^Basic^           ^Insert^        ^Save^         ^Edit^
╭─────────────────────────────────────────────────────────────────────────╯
     ^_i_^           [_e_] execute    [_n_] insert    [_b_] name      [_'_] previous
     ^^↑^^           [_d_] delete     [_t_] set       [_K_] key       [_,_] last
 _j_ ←   → _l_       [_o_] edit       [_a_] add       [_x_] register     
     ^^↓^^           [_r_] region     [_f_] format    [_B_] defun
     ^_k_^           [_m_] step
    ^^   ^^          [_s_] swap
"
  ("j" kmacro-start-macro :color blue)
  ("l" kmacro-end-or-call-macro-repeat)
  ("i" kmacro-cycle-ring-previous)
  ("k" kmacro-cycle-ring-next)
  ("r" apply-macro-to-region-lines)
  ("d" kmacro-delete-ring-head)
  ("e" kmacro-end-or-call-macro-repeat)
  ("o" kmacro-edit-macro-repeat)
  ("m" kmacro-step-edit-macro)
  ("s" kmacro-swap-ring)
  ("n" kmacro-insert-counter)
  ("t" kmacro-set-counter)
  ("a" kmacro-add-counter)
  ("f" kmacro-set-format)
  ("b" kmacro-name-last-macro)
  ("K" kmacro-bind-to-key)
  ("B" insert-kbd-macro)
  ("x" kmacro-to-register)
  ("'" kmacro-edit-macro)
  ("," edit-kbd-macro)
  ("q" nil :color blue))
;; Macro hydra:1 ends here

;; [[file:my-hydras.org::*hydra-gnus-group][hydra-gnus-group:1]]
(defhydra hydra-gnus-group (:hint nil :color pink)
  "
    gnus-group
╭─────────────────────────────────────────────────────────────────────────╯
 _c_ Catchup current (mark all articles in current group as read)
 _RET_ Read group (enter Summary buffer)

 Visibility:
 _L_ Show all groups
 _t_ Show topics (group categories)

 Server management:
 _B_ Browse foreign server (when you know the host name)
 _\\^_  Enter server mode (registered server host names)

 Hydra:
 _h_ Toggle verbose
 _q_ Exit
"
  ("RET" (progn             
           (gnus-topic-select-group)
           (hydra-gnus-summary/body)
           (hydra-push '(hydra-gnus-group/body)))
   "Read group")

  ("c" gnus-group-catchup-current "Catch-up all current")

  ("L" gnus-group-list-all-groups "List all groups")
  ("t" gnus-topic-mode "Toggle view topics" )

  ("B" gnus-group-browse-foreign-server "Browse foreign server"
   :exit t)
  ("^" gnus-group-enter-server-mode "Enter server buffer"
   :exit t)

  ("q" nil "Hydra, quit."
   :exit t)

  ("h" (my/hydra-toggle-verbosity 'hydra-gnus-group)
   "Hydra, toggle verbosity."
   :exit nil))
;; hydra-gnus-group:1 ends here

;; [[file:my-hydras.org::*Japanese hydras][Japanese hydras:1]]
(defun my/insert-unicode (unicode-name)
  "Same as C-x 8 enter UNICODE-NAME."
  (insert-char (gethash unicode-name (ucs-names))))

(defhydra hydra-japanese (:color blue :hint nil)
"
%s(all-the-icons-faicon \"flag\") Japanese
_h_ ひらがな/Hiragana
_k_ カタカナ/Katakana
_y_ 約物 (やくもの)/Yakumono/Puntuation
"
  ("h" hydra-hiragana/body "Japanese Hiragana")
  ("k" hydra-katakana/body "Japanese Katakana")
  ("y" hydra-yakumono/body "Japanese Yakumono"))
;; Japanese hydras:1 ends here

;; [[file:my-hydras.org::*Yakumono - Puntuation][Yakumono - Puntuation:1]]
(defhydra hydra-yakumono (:color pink :hint nil)
  "
Namikakko/Braces                          ^^   {  |  ^^   }      || _j_ Japanese hydra
Marukakko/Parentheses                     ^^   (  |  ^^   )      || _qq_ Quit 
Kakukakko/square brackets                 ^^   [  |  ^^   ]
Sumitsukikakko                            _sl_ 【  |  _sr_ 】

Touten                                    _c_ 、 
Comma                                     ^^  , 

Riidaa, tensen or ten-ten                 _ee_ … | _e2_ ‥
Kuten/Full stop                           _ss_ 。
%s(all-the-icons-faicon \"exclamation-circle\") Quotation marks
Kagikakko/Single quotation marks          _ql_   「  |  _qr_   」
Nijuukagikakko/Double quotation marks     _qdl_  『  |  _qdr_  』
Nijuukagikakko/Double quotation marks     _qddl_ 〝  |  _qddr_ 〟

Nakaguro/Interpunct                       _n_   ・
Prolonged sound mark                      _ps_  ー

Nami dasshu/Wave dash                     _wd_  〜
Musical note                              _mn_  ♪
Ioriten or utakigou/Part alternation mark _sa_  〽
Koron/Colon                               ^^    :
Katanfu/Exclamation mark                  ^^    !
Gimonfu or hatena maaku/Question mark     ^^    ?
"
  ;; ("qq" nil :exit t)
  ("sl" (my/insert-unicode "LEFT BLACK LENTICULAR BRACKET") "Left Sumitsukikakko")
  ("sr" (my/insert-unicode "RIGHT BLACK LENTICULAR BRACKET") "Left Sumitsukikakko")

  ("c"  (my/insert-unicode "IDEOGRAPHIC COMMA") "Touten")

  ("ee" (my/insert-unicode "HORIZONTAL ELLIPSIS") "tensen")
  ("e2" (my/insert-unicode "TWO DOT LEADER") "ten-ten")
  ("ss" (my/insert-unicode "IDEOGRAPHIC FULL STOP") "Kuten")

  ("ql" (my/insert-unicode "LEFT CORNER BRACKET") "Left Kagikakko")
  ("qr" (my/insert-unicode "RIGHT CORNER BRACKET") "Right Kagikakko")
  ("qdl" (my/insert-unicode "LEFT WHITE CORNER BRACKET") "Left nijuukagikakko")
  ("qdr" (my/insert-unicode "RIGHT WHITE CORNER BRACKET") "Left Nijuukagikakko")
  ("qddl" (my/insert-unicode "REVERSED DOUBLE PRIME QUOTATION MARK") "Left nijuukagikakko")
  ("qddr" (my/insert-unicode "LOW DOUBLE PRIME QUOTATION MARK") "Left nijuukagikakko")

  ("n" (my/insert-unicode "KATAKANA MIDDLE DOT") "Nakaguro")
  ("ps" (my/insert-unicode "KATAKANA-HIRAGANA PROLONGED SOUND MARK") "Prolonged sound mark")

  ("wd" (my/insert-unicode "WAVE DASH") "Nami dasshu")
  ("mn" (my/insert-unicode "EIGHTH NOTE") "Musical note")
  ("sa" (my/insert-unicode "PART ALTERNATION MARK") "Ioriten or Utakigou")
  ("j" hydra-japanese/body "Japanese hydra" :exit t)
  ("qq" nil :exit t))
;; Yakumono - Puntuation:1 ends here

;; [[file:my-hydras.org::*Katakana][Katakana:1]]
(defhydra hydra-katakana (:color pink :hint nil)
  "
  _a_  ア _i_  イ _u_  ウ _e_  エ _o_  オ |         dakuon

  _ka_ カ _ki_ キ _ku_ ク _ke_ ケ _ko_ コ | _ga_ ガ _gi_ ギ _gu_ グ _ge_ ゲ _go_ ゴ

  _sa_ サ _si_ シ _su_ ス _se_ セ _so_ ソ | _za_ ザ _zi_ ジ _zu_ ズ _ze_ ゼ _zo_ ゾ
  ^  ^    ^(shi)
  _ta_ タ _ti_ チ _tu_ ツ _te_ テ _to_ ト | _da_ ダ _di_ ヂ _du_ ヅ _de_ デ _do_ ド
  ^  ^    ^  ^   ^(tsu)
  _na_ ナ _ni_ ニ _nu_ ヌ _ne_ ネ _no_ ノ |

  _ha_ ハ _hi_ ヒ _hu_ フ _he_ ヘ _ho_ ホ | _ga_ ガ _gi_ ギ _gu_ グ _ge_ ゲ _go_ ゴ
  ^  ^    ^  ^   ^(fu)                             hadakuon
  _ma_ マ _mi_ ミ _mu_ ム _me_ メ _mo_ モ | _pa_ パ _pi_ ピ _pu_ プ _pe_ ペ _po_ ポ

  _ya_ ヤ ^  ^    _yu_ ユ ^  ^   _yo_ ヨ | 

  _ra_ ラ _ri_ リ _ru_ ル _re_ レ _ro_ ロ | 

  _wa_ ワ ^  ^    ^  ^   ^  ^    _wo_ ヲ | _nn_ ン

  _q_ quit | _j_ Japanese hydra
  "
  ;; ("e" (my/insert-unicode "EURO SIGN"))
  ;; ("r" (my/insert-unicode "MALE SIGN"))
  ;; ("f" (my/insert-unicode "FEMALE SIGN"))
  ;; ("s" (my/insert-unicode "ZERO WIDTH SPACE"))
  ;; ("o" (my/insert-unicode "DEGREE SIGN"))
  ;; ("a" (my/insert-unicode "RIGHTWARDS ARROW"))
  ;; ("m" (my/insert-unicode "MICRO SIGN"))

  ;;   ("<right>" (right-char))
  ;;   ("<left>" (left-char))
  ;;   ("<up>" (previous-line))
  ;;   ("<down>" (next-line))

  ("a" (my/insert-unicode "KATAKANA LETTER A"))
  ("i" (my/insert-unicode "KATAKANA LETTER I"))
  ("u" (my/insert-unicode "KATAKANA LETTER U"))
  ("e" (my/insert-unicode "KATAKANA LETTER E"))
  ("o" (my/insert-unicode "KATAKANA LETTER O"))

  ("ka" (my/insert-unicode "KATAKANA LETTER KA"))
  ("ki" (my/insert-unicode "KATAKANA LETTER KI"))
  ("ku" (my/insert-unicode "KATAKANA LETTER KU"))
  ("ke" (my/insert-unicode "KATAKANA LETTER KE"))
  ("ko" (my/insert-unicode "KATAKANA LETTER KO"))

  ("sa" (my/insert-unicode "KATAKANA LETTER SA"))
  ("si" (my/insert-unicode "KATAKANA LETTER SI"))
  ("su" (my/insert-unicode "KATAKANA LETTER SU"))
  ("se" (my/insert-unicode "KATAKANA LETTER SE"))
  ("so" (my/insert-unicode "KATAKANA LETTER SO"))

  ("ta" (my/insert-unicode "KATAKANA LETTER TA"))
  ("ti" (my/insert-unicode "KATAKANA LETTER TI"))
  ("tu" (my/insert-unicode "KATAKANA LETTER TU"))
  ("te" (my/insert-unicode "KATAKANA LETTER TE"))
  ("to" (my/insert-unicode "KATAKANA LETTER TO"))

  ("na" (my/insert-unicode "KATAKANA LETTER NA"))
  ("ni" (my/insert-unicode "KATAKANA LETTER NI"))
  ("nu" (my/insert-unicode "KATAKANA LETTER NU"))
  ("ne" (my/insert-unicode "KATAKANA LETTER NE"))
  ("no" (my/insert-unicode "KATAKANA LETTER NO"))

  ("ha" (my/insert-unicode "KATAKANA LETTER HA"))
  ("hi" (my/insert-unicode "KATAKANA LETTER HI"))
  ("hu" (my/insert-unicode "KATAKANA LETTER HU"))
  ("he" (my/insert-unicode "KATAKANA LETTER HE"))
  ("ho" (my/insert-unicode "KATAKANA LETTER HO"))

  ("ma" (my/insert-unicode "KATAKANA LETTER MA"))
  ("mi" (my/insert-unicode "KATAKANA LETTER MI"))
  ("mu" (my/insert-unicode "KATAKANA LETTER MU"))
  ("me" (my/insert-unicode "KATAKANA LETTER ME"))
  ("mo" (my/insert-unicode "KATAKANA LETTER MO"))

  ("ya" (my/insert-unicode "KATAKANA LETTER YA"))
  ("yu" (my/insert-unicode "KATAKANA LETTER YU"))
  ("yo" (my/insert-unicode "KATAKANA LETTER YO"))

  ("ra" (my/insert-unicode "KATAKANA LETTER RA"))
  ("ri" (my/insert-unicode "KATAKANA LETTER RI"))
  ("ru" (my/insert-unicode "KATAKANA LETTER RU"))
  ("re" (my/insert-unicode "KATAKANA LETTER RE"))
  ("ro" (my/insert-unicode "KATAKANA LETTER RO"))

  ("wa" (my/insert-unicode "KATAKANA LETTER WA"))
  ("wo" (my/insert-unicode "KATAKANA LETTER WO"))

  ("nn" (my/insert-unicode "KATAKANA LETTER N"))

  ;; dakuon

  ("ga" (my/insert-unicode "KATAKANA LETTER GA"))
  ("gi" (my/insert-unicode "KATAKANA LETTER GI"))
  ("gu" (my/insert-unicode "KATAKANA LETTER GU"))
  ("ge" (my/insert-unicode "KATAKANA LETTER GE"))
  ("go" (my/insert-unicode "KATAKANA LETTER GO"))

  ("za" (my/insert-unicode "KATAKANA LETTER ZA"))
  ("zi" (my/insert-unicode "KATAKANA LETTER ZI"))
  ("zu" (my/insert-unicode "KATAKANA LETTER ZU"))
  ("ze" (my/insert-unicode "KATAKANA LETTER ZE"))
  ("zo" (my/insert-unicode "KATAKANA LETTER ZO"))

  ("da" (my/insert-unicode "KATAKANA LETTER DA"))
  ("di" (my/insert-unicode "KATAKANA LETTER DI"))
  ("du" (my/insert-unicode "KATAKANA LETTER DU"))
  ("de" (my/insert-unicode "KATAKANA LETTER DE"))
  ("do" (my/insert-unicode "KATAKANA LETTER DO"))

  ("ba" (my/insert-unicode "KATAKANA LETTER BA"))
  ("bi" (my/insert-unicode "KATAKANA LETTER BI"))
  ("bu" (my/insert-unicode "KATAKANA LETTER BU"))
  ("be" (my/insert-unicode "KATAKANA LETTER BE"))
  ("bo" (my/insert-unicode "KATAKANA LETTER BO"))

  ;; hadakuon
  ("pa" (my/insert-unicode "KATAKANA LETTER PA"))
  ("pi" (my/insert-unicode "KATAKANA LETTER PI"))
  ("pu" (my/insert-unicode "KATAKANA LETTER PU"))
  ("pe" (my/insert-unicode "KATAKANA LETTER PE"))
  ("po" (my/insert-unicode "KATAKANA LETTER PO"))

  ;; Yuoon
  ("kya" (my/insert-unicode "KATAKANA LETTER KYA"))
  ("kyu" (my/insert-unicode "KATAKANA LETTER KYI"))
  ("kyo" (my/insert-unicode "KATAKANA LETTER KYU"))

  ("sha" (my/insert-unicode "KATAKANA LETTER SHA"))
  ("shu" (my/insert-unicode "KATAKANA LETTER SHU"))
  ("sho" (my/insert-unicode "KATAKANA LETTER SHO"))

  ("cha" (my/insert-unicode "KATAKANA LETTER CHA"))
  ("chu" (my/insert-unicode "KATAKANA LETTER CHU"))
  ("cho" (my/insert-unicode "KATAKANA LETTER CHO"))

  ;; ("nya" (my/insert-unicode "KATAKANA LETTER NYA"))
  ;; ("nyu" (my/insert-unicode "KATAKANA LETTER NYU"))
  ;; ("nyo" (my/insert-unicode "KATAKANA LETTER NYO"))

  ("hya" (my/insert-unicode "KATAKANA LETTER HYA"))
  ("hyu" (my/insert-unicode "KATAKANA LETTER HYU"))
  ("hyo" (my/insert-unicode "KATAKANA LETTER HYO"))

  ("mya" (my/insert-unicode "KATAKANA LETTER MYA"))
  ("myu" (my/insert-unicode "KATAKANA LETTER MYU"))
  ("myo" (my/insert-unicode "KATAKANA LETTER MYO"))

  ("rya" (my/insert-unicode "KATAKANA LETTER RYA"))
  ("ryu" (my/insert-unicode "KATAKANA LETTER RYU"))
  ("ryo" (my/insert-unicode "KATAKANA LETTER RYO"))

  ("ja" (my/insert-unicode "KATAKANA LETTER JA"))
  ("ju" (my/insert-unicode "KATAKANA LETTER JU"))
  ("jo" (my/insert-unicode "KATAKANA LETTER JO"))

  ("dja" (my/insert-unicode "KATAKANA LETTER DJA"))
  ("dju" (my/insert-unicode "KATAKANA LETTER DJU"))
  ("djo" (my/insert-unicode "KATAKANA LETTER DJO"))

  ("bya" (my/insert-unicode "KATAKANA LETTER BYA"))
  ("byu" (my/insert-unicode "KATAKANA LETTER BYU"))
  ("byo" (my/insert-unicode "KATAKANA LETTER BYO"))

  ("pya" (my/insert-unicode "KATAKANA LETTER PYA"))
  ("pyu" (my/insert-unicode "KATAKANA LETTER PYU"))
  ("pyo" (my/insert-unicode "KATAKANA LETTER PYO"))
  ("j" hydra-japanese/body "Japanese hydra" :exit t)
  ("q" nil :exit t))
;; Katakana:1 ends here

;; [[file:my-hydras.org::*Hiragana][Hiragana:1]]
(defhydra hydra-hiragana (:color pink :hint nil)
   "
_a_  あ _i_  い _u_  う _e_  え _o_  お |         dakuon

_ka_ か _ki_ き _ku_ く _ke_ け _ko_ こ | _ga_ が _gi_ ぎ _gu_ ぐ _ge_ げ _go_ ご

_sa_ さ _si_ し _su_ す _se_ せ _so_ そ | _za_ ざ _zi_ じ _zu_ ず _ze_ ぜ _zo_ ぞ
^  ^    ^(shi)
_ta_ た _ti_ ち _tu_ つ _te_ て _to_ と | _da_ だ _di_ ぢ _du_ づ _de_ で _do_ ど
^  ^    ^  ^   ^(tsu)
_na_ な _ni_ に _nu_ ぬ _ne_ ね _no_ の |

_ha_ は _hi_ ひ _hu_ ふ _he_ へ _ho_ ほ | _ga_ ば _gi_ び _gu_ ぶ _ge_ べ _go_ ぼ
^  ^    ^  ^   ^(fu)                             hadakuon
_ma_ ま _mi_ み _mu_ む _me_ め _mo_ も | _pa_ ぱ _pi_ ぴ _pu_ ぷ _pe_ ぺ _po_ ぽ

_ya_ や ^  ^    _yu_ ゆ ^  ^   _yo_ よ | 

_ra_ ら _ri_ り _ru_ る _re_ れ _ro_ ろ | 

_wa_ わ ^  ^   ^  ^    ^  ^    _wo_ を | _nn_ ん

_q_ quit | _j_ Japanese hydra
"
   ;; ("e" (my/insert-unicode "EURO SIGN"))
   ;; ("r" (my/insert-unicode "MALE SIGN"))
   ;; ("f" (my/insert-unicode "FEMALE SIGN"))
   ;; ("s" (my/insert-unicode "ZERO WIDTH SPACE"))
   ;; ("o" (my/insert-unicode "DEGREE SIGN"))
   ;; ("a" (my/insert-unicode "RIGHTWARDS ARROW"))
   ;; ("m" (my/insert-unicode "MICRO SIGN"))

   ;;   ("<right>" (right-char))
   ;;   ("<left>" (left-char))
   ;;   ("<up>" (previous-line))
   ;;   ("<down>" (next-line))

   ("a" (my/insert-unicode "HIRAGANA LETTER A"))
   ("i" (my/insert-unicode "HIRAGANA LETTER I"))
   ("u" (my/insert-unicode "HIRAGANA LETTER U"))
   ("e" (my/insert-unicode "HIRAGANA LETTER E"))
   ("o" (my/insert-unicode "HIRAGANA LETTER O"))

   ("ka" (my/insert-unicode "HIRAGANA LETTER KA"))
   ("ki" (my/insert-unicode "HIRAGANA LETTER KI"))
   ("ku" (my/insert-unicode "HIRAGANA LETTER KU"))
   ("ke" (my/insert-unicode "HIRAGANA LETTER KE"))
   ("ko" (my/insert-unicode "HIRAGANA LETTER KO"))

   ("sa" (my/insert-unicode "HIRAGANA LETTER SA"))
   ("si" (my/insert-unicode "HIRAGANA LETTER SI"))
   ("su" (my/insert-unicode "HIRAGANA LETTER SU"))
   ("se" (my/insert-unicode "HIRAGANA LETTER SE"))
   ("so" (my/insert-unicode "HIRAGANA LETTER SO"))

   ("ta" (my/insert-unicode "HIRAGANA LETTER TA"))
   ("ti" (my/insert-unicode "HIRAGANA LETTER TI"))
   ("tu" (my/insert-unicode "HIRAGANA LETTER TU"))
   ("te" (my/insert-unicode "HIRAGANA LETTER TE"))
   ("to" (my/insert-unicode "HIRAGANA LETTER TO"))

   ("na" (my/insert-unicode "HIRAGANA LETTER NA"))
   ("ni" (my/insert-unicode "HIRAGANA LETTER NI"))
   ("nu" (my/insert-unicode "HIRAGANA LETTER NU"))
   ("ne" (my/insert-unicode "HIRAGANA LETTER NE"))
   ("no" (my/insert-unicode "HIRAGANA LETTER NO"))

   ("ha" (my/insert-unicode "HIRAGANA LETTER HA"))
   ("hi" (my/insert-unicode "HIRAGANA LETTER HI"))
   ("hu" (my/insert-unicode "HIRAGANA LETTER HU"))
   ("he" (my/insert-unicode "HIRAGANA LETTER HE"))
   ("ho" (my/insert-unicode "HIRAGANA LETTER HO"))

   ("ma" (my/insert-unicode "HIRAGANA LETTER MA"))
   ("mi" (my/insert-unicode "HIRAGANA LETTER MI"))
   ("mu" (my/insert-unicode "HIRAGANA LETTER MU"))
   ("me" (my/insert-unicode "HIRAGANA LETTER ME"))
   ("mo" (my/insert-unicode "HIRAGANA LETTER MO"))

   ("ya" (my/insert-unicode "HIRAGANA LETTER YA"))
   ("yu" (my/insert-unicode "HIRAGANA LETTER YU"))
   ("yo" (my/insert-unicode "HIRAGANA LETTER YO"))

   ("ra" (my/insert-unicode "HIRAGANA LETTER RA"))
   ("ri" (my/insert-unicode "HIRAGANA LETTER RI"))
   ("ru" (my/insert-unicode "HIRAGANA LETTER RU"))
   ("re" (my/insert-unicode "HIRAGANA LETTER RE"))
   ("ro" (my/insert-unicode "HIRAGANA LETTER RO"))

   ("wa" (my/insert-unicode "HIRAGANA LETTER WA"))
   ("wo" (my/insert-unicode "HIRAGANA LETTER WO"))

   ("nn" (my/insert-unicode "HIRAGANA LETTER N"))

   ;; dakuon

   ("ga" (my/insert-unicode "HIRAGANA LETTER GA"))
   ("gi" (my/insert-unicode "HIRAGANA LETTER GI"))
   ("gu" (my/insert-unicode "HIRAGANA LETTER GU"))
   ("ge" (my/insert-unicode "HIRAGANA LETTER GE"))
   ("go" (my/insert-unicode "HIRAGANA LETTER GO"))

   ("za" (my/insert-unicode "HIRAGANA LETTER ZA"))
   ("zi" (my/insert-unicode "HIRAGANA LETTER ZI"))
   ("zu" (my/insert-unicode "HIRAGANA LETTER ZU"))
   ("ze" (my/insert-unicode "HIRAGANA LETTER ZE"))
   ("zo" (my/insert-unicode "HIRAGANA LETTER ZO"))

   ("da" (my/insert-unicode "HIRAGANA LETTER DA"))
   ("di" (my/insert-unicode "HIRAGANA LETTER DI"))
   ("du" (my/insert-unicode "HIRAGANA LETTER DU"))
   ("de" (my/insert-unicode "HIRAGANA LETTER DE"))
   ("do" (my/insert-unicode "HIRAGANA LETTER DO"))

   ("ba" (my/insert-unicode "HIRAGANA LETTER BA"))
   ("bi" (my/insert-unicode "HIRAGANA LETTER BI"))
   ("bu" (my/insert-unicode "HIRAGANA LETTER BU"))
   ("be" (my/insert-unicode "HIRAGANA LETTER BE"))
   ("bo" (my/insert-unicode "HIRAGANA LETTER BO"))

   ;; hadakuon
   ("pa" (my/insert-unicode "HIRAGANA LETTER PA"))
   ("pi" (my/insert-unicode "HIRAGANA LETTER PI"))
   ("pu" (my/insert-unicode "HIRAGANA LETTER PU"))
   ("pe" (my/insert-unicode "HIRAGANA LETTER PE"))
   ("po" (my/insert-unicode "HIRAGANA LETTER PO"))

   ;; Yuoon
   ("kya" (my/insert-unicode "HIRAGANA LETTER KYA"))
   ("kyu" (my/insert-unicode "HIRAGANA LETTER KYI"))
   ("kyo" (my/insert-unicode "HIRAGANA LETTER KYU"))

   ("sha" (my/insert-unicode "HIRAGANA LETTER SHA"))
   ("shu" (my/insert-unicode "HIRAGANA LETTER SHU"))
   ("sho" (my/insert-unicode "HIRAGANA LETTER SHO"))

   ("cha" (my/insert-unicode "HIRAGANA LETTER CHA"))
   ("chu" (my/insert-unicode "HIRAGANA LETTER CHU"))
   ("cho" (my/insert-unicode "HIRAGANA LETTER CHO"))

   ;; ("nya" (my/insert-unicode "HIRAGANA LETTER NYA"))
   ;; ("nyu" (my/insert-unicode "HIRAGANA LETTER NYU"))
   ;; ("nyo" (my/insert-unicode "HIRAGANA LETTER NYO"))

   ("hya" (my/insert-unicode "HIRAGANA LETTER HYA"))
   ("hyu" (my/insert-unicode "HIRAGANA LETTER HYU"))
   ("hyo" (my/insert-unicode "HIRAGANA LETTER HYO"))

   ("mya" (my/insert-unicode "HIRAGANA LETTER MYA"))
   ("myu" (my/insert-unicode "HIRAGANA LETTER MYU"))
   ("myo" (my/insert-unicode "HIRAGANA LETTER MYO"))

   ("rya" (my/insert-unicode "HIRAGANA LETTER RYA"))
   ("ryu" (my/insert-unicode "HIRAGANA LETTER RYU"))
   ("ryo" (my/insert-unicode "HIRAGANA LETTER RYO"))

   ("ja" (my/insert-unicode "HIRAGANA LETTER JA"))
   ("ju" (my/insert-unicode "HIRAGANA LETTER JU"))
   ("jo" (my/insert-unicode "HIRAGANA LETTER JO"))

   ("dja" (my/insert-unicode "HIRAGANA LETTER DJA"))
   ("dju" (my/insert-unicode "HIRAGANA LETTER DJU"))
   ("djo" (my/insert-unicode "HIRAGANA LETTER DJO"))

   ("bya" (my/insert-unicode "HIRAGANA LETTER BYA"))
   ("byu" (my/insert-unicode "HIRAGANA LETTER BYU"))
   ("byo" (my/insert-unicode "HIRAGANA LETTER BYO"))

   ("pya" (my/insert-unicode "HIRAGANA LETTER PYA"))
   ("pyu" (my/insert-unicode "HIRAGANA LETTER PYU"))
   ("pyo" (my/insert-unicode "HIRAGANA LETTER PYO"))

   ("j" hydra-japanese/body "Japanese hydra" :exit t)
   ("q" nil :exit t))
;; Hiragana:1 ends here

;; [[file:my-hydras.org::*Org-mode hydras][Org-mode hydras:1]]
(defhydra hydra-org-mode (:color blue :hint nil)
"
%s(all-the-icons-faicon \"pencil\") Org-mode
_a_ org-agenda
_b_ org-brain
"
  ("b" hydra-org-brain/body "Org brain")
  ("a" hydra-org-agenda/body "Org agenda"))
;; Org-mode hydras:1 ends here

;; [[file:my-hydras.org::*org-brain hydra][org-brain hydra:1]]
(defhydra hydra-org-brain (:color pink :hint nil)
    "
Movement
_O_ Goto           | _b_ Visualize back   | ^  _u_
_v_ Visualize node | _w_ Visualize random | _k_  _j_   
_o_ Goto org-file  |^^                    | ^^ ENTER 

_z_ / _Z_ Show/hide ancestor   | _V_ Visualize follow
_+_ / _-_ Show/hide descendant | _W_ Visualize wander
_m_ Mind-map^^                 | _s_ Select DWIM

Add^^^^              | Remove^^         || _A_ archive
^^^^                 | _d_ Entry        || 
_h_ _*_ Child header | _P_ Parent       || _T_ set tags
_c_ ^^  Child file   | _C_ Child        || _t_ set title
_f_ ^^  Friendship   | _F_ Frienship    ||
_N_ ^^  Nickname     +^^----------------++---------------
_n_ Add Pin            _a_ Attachment      _e_ Annotate edge
_l_ Resource (link)    _r_ Open resource

_qb_ Quit org brain
_qq_ Quit hydra
"


    ("*" org-brain-add-child-headline)
    ("+" org-brain-show-descendant-level)
    ("-" org-brain-hide-descendant-level)
    ("A" org-brain-archive)
    ("C" org-brain-remove-child)
    ("F" org-brain-remove-friendship)

    ;; M org-brain-move-map
    ("Mp" org-brain-change-local-parent)
    ("Mr" org-brain-refile)

    ("N" org-brain-add-nickname)
    ("O" org-brain-goto)
    ("P" org-brain-remove-parent)

    ;; ("S" org-brain-select-map)
    ("SC" org-brain-remove-selected-children)
    ("SF" org-brain-remove-selected-friendships)
    ("SP" org-brain-remove-selected-parents)
    ("SS" org-brain-clear-selected)
    ("Sc" org-brain-add-selected-children)
    ("Sd" org-brain-delete-selected-entries)
    ("Sf" org-brain-add-selected-friendships)
    ("Sl" org-brain-change-selected-local-parents)
    ("Sp" org-brain-add-selected-parents)
    ("Ss" org-brain-clear-selected)

    ("T" org-brain-set-tags)
    ("V" org-brain-visualize-follow)
    ("W" org-brain-visualize-wander)
    ("Z" org-brain-hide-ancestor-level)
    ("a" org-brain-visualize-attach)
    ("b" org-brain-visualize-back)
    ("c" org-brain-add-child)
    ("d" org-brain-delete-entry)
    ("e" org-brain-annotate-edge)
    ("f" org-brain-add-friendship)
    ("g" revert-buffer)
    ("h" org-brain-add-child-headline)
    ("j" forward-button)
    ("k" backward-button)
    ("l" org-brain-add-resource)
    ("m" org-brain-visualize-mind-map)
    ("n" org-brain-pin)
    ("o" org-brain-goto-current)
    ("p" org-brain-add-parent)
    ("qb" org-brain-visualize-quit :exit t)
    ("r" org-brain-open-resource)
    ("s" org-brain-select-dwim)
    ("t" org-brain-set-title)
    ("u" org-brain-visualize-parent)
    ("v" org-brain-visualize)
    ("w" org-brain-visualize-random)
    ("z" org-brain-show-ancestor-level)
    ("qq" nil :exit t))
;; org-brain hydra:1 ends here

;; [[file:my-hydras.org::*org-agenda hydra][org-agenda hydra:1]]
;; Hydra for org agenda (graciously taken from Spacemacs)
(defhydra hydra-org-agenda (:pre (setq which-key-inhibit t)
                                 :post (setq which-key-inhibit nil)
                                 :hint none)
  "
Org agenda (_q_uit)

^Clock^      ^Visit entry^              ^Date^             ^Other^
^-----^----  ^-----------^------------  ^----^-----------  ^-----^---------
_ci_ in      _SPC_ in other window      _ds_ schedule      _gr_ reload
_co_ out     _TAB_ & go to location     _dd_ set deadline  _._  go to today
_cq_ cancel  _RET_ & del other windows  _dt_ timestamp     _gd_ go to date
_cj_ jump    _o_   link                 _+_  do later      ^^
^^           ^^                         _-_  do earlier    ^^
^^           ^^                         ^^                 ^^
^View^          ^Filter^                 ^Headline^         ^Toggle mode^
^----^--------  ^------^---------------  ^--------^-------  ^-----------^----
_vd_ day        _ft_ by tag              _ht_ set status    _tf_ follow
_vw_ week       _fr_ refine by tag       _hk_ kill          _tl_ log
_vt_ fortnight  _fc_ by category         _hr_ refile        _ta_ archive trees
_vm_ month      _fh_ by top headline     _hA_ archive       _tA_ archive files
_vy_ year       _fx_ by regexp           _h:_ set tags      _tr_ clock report
_vn_ next span  _fd_ delete all filters  _hp_ set priority  _td_ diaries
_vp_ prev span  ^^                       ^^                 ^^
_vr_ reset      ^^                       ^^                 ^^
^^              ^^                       ^^                 ^^
"
  ;; Entry
  ("hA" org-agenda-archive-default)
  ("hk" org-agenda-kill)
  ("hp" org-agenda-priority)
  ("hr" org-agenda-refile)
  ("h:" org-agenda-set-tags)
  ("ht" org-agenda-todo)
  ;; Visit entry
  ("o"   link-hint-open-link :exit t)
  ("<tab>" org-agenda-goto :exit t)
  ("TAB" org-agenda-goto :exit t)
  ("SPC" org-agenda-show-and-scroll-up)
  ("RET" org-agenda-switch-to :exit t)
  ;; Date
  ("dt" org-agenda-date-prompt)
  ("dd" org-agenda-deadline)
  ("+" org-agenda-do-date-later)
  ("-" org-agenda-do-date-earlier)
  ("ds" org-agenda-schedule)
  ;; View
  ("vd" org-agenda-day-view)
  ("vw" org-agenda-week-view)
  ("vt" org-agenda-fortnight-view)
  ("vm" org-agenda-month-view)
  ("vy" org-agenda-year-view)
  ("vn" org-agenda-later)
  ("vp" org-agenda-earlier)
  ("vr" org-agenda-reset-view)
  ;; Toggle mode
  ("ta" org-agenda-archives-mode)
  ("tA" (org-agenda-archives-mode 'files))
  ("tr" org-agenda-clockreport-mode)
  ("tf" org-agenda-follow-mode)
  ("tl" org-agenda-log-mode)
  ("td" org-agenda-toggle-diary)
  ;; Filter
  ("fc" org-agenda-filter-by-category)
  ("fx" org-agenda-filter-by-regexp)
  ("ft" org-agenda-filter-by-tag)
  ("fr" org-agenda-filter-by-tag-refine)
  ("fh" org-agenda-filter-by-top-headline)
  ("fd" org-agenda-filter-remove-all)
  ;; Clock
  ("cq" org-agenda-clock-cancel)
  ("cj" org-agenda-clock-goto :exit t)
  ("ci" org-agenda-clock-in :exit t)
  ("co" org-agenda-clock-out)
  ;; Other
  ("q" nil :exit t)
  ("gd" org-agenda-goto-date)
  ("." org-agenda-goto-today)
  ("gr" org-agenda-redo))
;; org-agenda hydra:1 ends here

;; [[file:my-hydras.org::*hydra-wl-folder][hydra-wl-folder:1]]
(defhydra hydra-wl-folder (:hint nil :color pink)
  "
_e_ Expire current entity.

_q_ Quit hydra.
"
  ("e" wl-folder-expire-current-entity)

  ("q" nil :color blue))
;; hydra-wl-folder:1 ends here

;; [[file:my-hydras.org::*hydra-wl-draft][hydra-wl-draft:1]]
(defhydra hydra-wl-draft (:hint nil :color blue)
  "
_a_ Open agenda (C-c a).
_<tab>_ Attach file (C-c C-x <tab>).

_q_ Quit hydra.
"
  ("a" wl-addrmgr)
  ("<tab>" mime-edit-attach-file)
  ("q" nil :color blue))
;; hydra-wl-draft:1 ends here
