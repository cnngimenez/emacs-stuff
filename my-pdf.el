;; [[file:my-pdf.org::*Proporciones][Proporciones:1]]
(defun my-pdf-get-relative-position ()
  "Get the relative position from the mouse click."
  (interactive)
  (posn-object-x-y (pdf-util-read-image-position "Click para obtener la posición relativa")))
;; Proporciones:1 ends here

;; [[file:my-pdf.org::*Proporción X][Proporción X:1]]
(defconst my-pdf-x-proportion 0.0019377162629757784
  "X proportion between PDF and relative coordinates.
Proportion between PDF left/right coordinates and current page
coordinates with reseted scale.")
;; Proporción X:1 ends here

;; [[file:my-pdf.org::*Proporción Y][Proporción Y:1]]
(defconst my-pdf-y-proportion 0.001220730538129115
  "Y proportion between PDF and relative coordinates.
Proportion between PDF left/right coordinates and current page
coordinates with reseted scale.")
;; Proporción Y:1 ends here

;; [[file:my-pdf.org::*Convertir coordenadas PDF a relativas][Convertir coordenadas PDF a relativas:1]]
(defun my-pdf-coord-to-relative (list)
  "Convertir coordenadas PDF a relativas a la página."
  (let ((left (car list))
        (top (nth 1 list))
        (right (nth 2 list))
        (bottom (nth 3 list)))
    (list (* left my-pdf-x-proportion)
          (* top my-pdf-y-proportion)
          (* right my-pdf-x-proportion)
          (* bottom my-pdf-y-proportion))))
;; Convertir coordenadas PDF a relativas:1 ends here

;; [[file:my-pdf.org::*Al comienzo de un texto en el PDF][Al comienzo de un texto en el PDF:1]]
(defun my-pdf-annotate-text (string-to-search text)
  (interactive "MString to search? \nMText? ")
  (pdf-view-scale-reset)
  (let ((pdf-coord (caar (pdf-isearch-search-page string-to-search))))
    (pdf-annot-add-text-annotation (cons (car pdf-coord)
                                         (cadr pdf-coord))
                                   nil
                                   (list (cons 'label "christian")
                                         (cons 'contents text)))))

(defun my-pdf-highlight-text (string-to-search)
  (interactive "MString to search? ")
  (pdf-view-scale-reset)
  (let* ((pdf-coord (caar (pdf-isearch-search-page string-to-search)))
         (rel-coord (my-pdf-coord-to-relative pdf-coord)))
    (when pdf-coord
      (pdf-annot-add-highlight-markup-annotation rel-coord))))
;; Al comienzo de un texto en el PDF:1 ends here
